package net.mcparadise.deathswaps;

import me.chasertw123.minigames.core.api.CoreAPI;
import me.chasertw123.minigames.shared.framework.ServerGameType;
import me.chasertw123.minigames.shared.framework.ServerType;
import net.mcparadise.deathswaps.game.GameManager;
import net.mcparadise.deathswaps.game.MapManager;
import net.mcparadise.deathswaps.listeners.*;
import net.mcparadise.deathswaps.users.UserManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private static Main plugin;

    private static UserManager userManager;
    private static GameManager gameManager;
    private static MapManager mapManager;

    public static final int MIN_PLAYERS = 2, MAX_PLAYERS = 25;

    @Override
    public void onLoad() {
        CoreAPI.getServerDataManager().setServerGameType(ServerGameType.DEATH_SWAPS);
        CoreAPI.getServerDataManager().setServerType(ServerType.MINIGAME);
    }

    @Override
    public void onEnable() {
        plugin = this;

        userManager = new UserManager();
        mapManager = new MapManager();
        gameManager = new GameManager();


        this.getServer().getPluginManager().registerEvents(new Event_PlayerJoin(), this);
        this.getServer().getPluginManager().registerEvents(new Event_PlayerLeave(), this);
        this.getServer().getPluginManager().registerEvents(new Event_PlayerDeath(), this);
        this.getServer().getPluginManager().registerEvents(new Event_EntityDamage(), this);
        this.getServer().getPluginManager().registerEvents(new Event_EntityDeath(), this);
        this.getServer().getPluginManager().registerEvents(new Event_PlayerToggleFlight(), this);
        this.getServer().getPluginManager().registerEvents(new Event_EntityDamageByEntity(), this);
        this.getServer().getPluginManager().registerEvents(new Event_ChunkUnload(), this);
        this.getServer().getPluginManager().registerEvents(new Event_BlockPlace(), this);
    }

    @Override
    public void onDisable() {
        plugin = null;
    }

    public static Main getInstance() {
        return plugin;
    }

    public static GameManager getGameManager() {
        return gameManager;
    }

    public static UserManager getUserManager() {
        return userManager;
    }

    public static MapManager getMapManager() {
        return mapManager;
    }
}
