package net.mcparadise.deathswaps.listeners;

import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.game.GameState;
import net.mcparadise.deathswaps.users.DeathSwapUser;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class Event_PlayerInteract implements Listener {

    @EventHandler (priority = EventPriority.LOW)
    public void onPlayerInteract(PlayerInteractEvent event) {

        if (event.isCancelled() || Main.getGameManager().getGameState() != GameState.GAME)
            return;

        DeathSwapUser user = Main.getUserManager().get(event.getPlayer().getUniqueId());
        if (user.isAlive() && user.getPlayer().getItemInHand().getType().toString().contains("PICKAXE")) {

        }

    }

}
