package net.mcparadise.deathswaps.listeners;

import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.game.GameState;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleFlightEvent;

public class Event_PlayerToggleFlight implements Listener {

    @EventHandler
    public void onToggleFlight(PlayerToggleFlightEvent event) {
        if (Main.getGameManager().getGameState() == GameState.GAME
                && Main.getUserManager().get(event.getPlayer().getUniqueId()).isAlive()
                && event.getPlayer().getGameMode() != GameMode.CREATIVE)
            event.setCancelled(true);
    }
}
