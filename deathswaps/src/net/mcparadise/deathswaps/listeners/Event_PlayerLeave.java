package net.mcparadise.deathswaps.listeners;

import me.chasertw123.minigames.core.api.minigames.MinigameUtils;
import me.chasertw123.minigames.core.api.CoreAPI;
import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.users.DeathSwapUser;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Event_PlayerLeave implements Listener {

    @EventHandler
    public void onPlayerQuit(me.chasertw123.minigames.core.event.UserQuitEvent event) {

//        DeathSwapUser user = Main.getUserManager().get(event.getUser().getUUID());
//        if (!user.save())
//            Bukkit.getLogger().severe("Failed to save " + user.getPlayer().getName() + "'s data!");
//
//        else
//            System.out.println("Saved " + user.getPlayer().getName() + "'s Data!");

        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () ->
                CoreAPI.getServerDataManager().updateServerState(null, Main.MAX_PLAYERS), 20 * 5);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent e) {

        e.setQuitMessage(null); // TODO: Change leave message depending on state of the game

        DeathSwapUser user = Main.getUserManager().get(e.getPlayer().getUniqueId());
        if (user.isAlive()) {
            user.getCoreUser().addActivity(MinigameUtils.convertIntToOrdinal(Main.getGameManager().getAlive().size()) + " Death Swaps");
            user.setAlive(false);
        }

//        if (Main.getGameManager().getGameState() != GameState.LOBBY && user.isAlive())
//            user.getCoreUser().incrementStat(Stat.DEATHSWAPS_PLAYTIME, (int) (System.currentTimeMillis() - Main.getGameManager().getStartTime()) / 1000);

        Main.getUserManager().remove(e.getPlayer().getUniqueId());
    }

}
