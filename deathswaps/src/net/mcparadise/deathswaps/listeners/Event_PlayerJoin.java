package net.mcparadise.deathswaps.listeners;

import me.chasertw123.minigames.core.api.minigames.MinigameUtils;
import me.chasertw123.minigames.core.api.CoreAPI;
import me.chasertw123.minigames.core.api.minigames.spectating.SpectateAPI;
import me.chasertw123.minigames.core.api.misc.AbstractItem;
import me.chasertw123.minigames.core.api.misc.Item;
import me.chasertw123.minigames.core.api.misc.cItemStack;
import me.chasertw123.minigames.shared.framework.ServerSetting;
import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.game.GameState;
import net.mcparadise.deathswaps.game.extras.guis.GUI_Skills;
import net.mcparadise.deathswaps.game.scoreboards.SB_Game;
import net.mcparadise.deathswaps.game.scoreboards.SB_Lobby;
import net.mcparadise.deathswaps.users.DeathSwapUser;
import net.minecraft.server.v1_8_R3.Items;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.List;
import java.util.Random;

public class Event_PlayerJoin implements Listener {

    private Random rand = new Random();

    AbstractItem skillSelect = new AbstractItem(new cItemStack(Material.REDSTONE, ChatColor.YELLOW + "Skill Selector " + ChatColor.GRAY + "(Right Click)"),
                 (user, clickType) -> new GUI_Skills(Main.getUserManager().get(user.getUUID())));

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        if(Main.getGameManager().getGameState() == GameState.LOBBY && Bukkit.getServer().getOnlinePlayers().size() == Main.MAX_PLAYERS + 1) {
            event.getPlayer().kickPlayer("This server is at max capacity and not yet in game to spectate!");

            return;
        }

        DeathSwapUser deathSwapUser = new DeathSwapUser(event.getPlayer().getUniqueId());
        Main.getUserManager().add(deathSwapUser);

        event.getPlayer().setPlayerListName(deathSwapUser.getCoreUser().getRank().getRankColor() + event.getPlayer().getName());

        if (Main.getGameManager().getGameState() != GameState.LOBBY)
        {
            event.setJoinMessage(null);

            deathSwapUser.setAlive(false);
            deathSwapUser.getCoreUser().setScoreboard(new SB_Game(deathSwapUser));

            SpectateAPI.setSpectating(deathSwapUser.getCoreUser());

            List<DeathSwapUser> alive = Main.getGameManager().getAlive();
            deathSwapUser.getPlayer().teleport(alive.get(rand.nextInt(alive.size())).getPlayer());
            return;
        }

        net.md_5.bungee.api.ChatColor nameColor = deathSwapUser.getCoreUser().getRank().getRankColor();
        if (!deathSwapUser.getCoreUser().getRank().isStaff() && deathSwapUser.getCoreUser().isDeluxe())
            nameColor = net.md_5.bungee.api.ChatColor.GOLD;

        event.setJoinMessage(nameColor + event.getPlayer().getName() + ChatColor.GRAY + " joined the game! ("
                + Main.getUserManager().toCollection().size() + "/" + Main.MAX_PLAYERS + ")");

        deathSwapUser.getCoreUser().setScoreboard(new SB_Lobby(deathSwapUser));
        deathSwapUser.getCoreUser().setServerSetting(ServerSetting.DAMAGE, false);
        deathSwapUser.getCoreUser().setServerSetting(ServerSetting.HUNGER, false);
        deathSwapUser.getCoreUser().setServerSetting(ServerSetting.ITEM_DROPPING, false);

        MinigameUtils.resetPlayer(deathSwapUser.getPlayer());
        MinigameUtils.showAllPlayers();
        deathSwapUser.getPlayer().spigot().setCollidesWithEntities(true);
        event.getPlayer().teleport(Main.getMapManager().getLobby().getSpawnLocation().clone().add(0.5d, 0, 0.5d));

        Item.HOW_TO_PLAY.give(event.getPlayer(), 0);
        skillSelect.give(deathSwapUser.getPlayer(), 1);
        Item.RETURN_TO_HUB.give(deathSwapUser.getPlayer(), 8);

        CoreAPI.getServerDataManager().updateServerState(null, Main.MAX_PLAYERS);
    }
}
