package net.mcparadise.deathswaps.listeners;

import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.game.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;

public class Event_ChunkUnload implements Listener {

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent event) {
        if (Main.getGameManager().getGameState() == GameState.GAME && Main.getGameManager().isTeleporting())
            event.setCancelled(true);
    }

}
