package net.mcparadise.deathswaps.listeners;

import me.chasertw123.minigames.core.api.minigames.MinigameUtils;
import me.chasertw123.minigames.core.api.minigames.spectating.SpectateAPI;
import me.chasertw123.minigames.core.api.misc.Title;
import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.game.GameState;
import net.mcparadise.deathswaps.users.DeathSwapUser;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class Event_EntityDeath implements Listener {

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        if (Main.getGameManager().getGameState() != GameState.GAME)
            return;

        if (event.getEntityType() != EntityType.ENDER_DRAGON)
            return;

        Main.getGameManager().endGame(Main.getUserManager().get(event.getEntity().getKiller().getUniqueId()));
        Bukkit.getOnlinePlayers().forEach(player -> {
            if (player.getUniqueId() != event.getEntity().getUniqueId()) {
                DeathSwapUser user = Main.getUserManager().get(player.getUniqueId());
                user.setAlive(false);
                user.getPlayer().teleport(event.getEntity().getLocation());

                Title.sendSubtitle(player, 0, 20 , 0, event.getEntity().getKiller().getName() + ChatColor.GRAY + " has slain the " + ChatColor.RED + "Ender Dragon!");
                SpectateAPI.setSpectating(user.getCoreUser());
            }
        });

        Title.sendTitles(event.getEntity().getKiller(), 0, 20, 0, ChatColor.GOLD + "YOU WIN!", ChatColor.GRAY + "You have slain the Ender Dragon!");
    }
}
