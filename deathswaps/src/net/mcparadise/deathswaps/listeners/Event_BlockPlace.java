package net.mcparadise.deathswaps.listeners;

import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.game.GameState;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class Event_BlockPlace implements Listener {

    @EventHandler (priority = EventPriority.HIGH)
    public void onBlockPlace(BlockPlaceEvent event) {

        if (event.isCancelled() || Main.getGameManager().getGameState() != GameState.GAME || Main.getGameManager().canTower())
            return;

        if (!isTowering(event.getBlock()))
            return;

        event.getPlayer().sendMessage(ChatColor.RED + "Towering is disabled before the first teleport!");
        event.setCancelled(true);
    }

    private boolean isTowering(Block block) {
        int height = 0;
        Block currentBlock = block;
        while (isPartOfTower(currentBlock)) {
            currentBlock = currentBlock.getRelative(BlockFace.DOWN);
            if (currentBlock.getType() == Material.AIR)
                break;

            if (++height >= 10)
                return true;
        }

        return false;
    }

    private boolean isPartOfTower(Block block) {
        return block.getRelative(BlockFace.EAST).getType() == Material.AIR &&
                block.getRelative(BlockFace.WEST).getType() == Material.AIR &&
                block.getRelative(BlockFace.NORTH).getType() == Material.AIR &&
                block.getRelative(BlockFace.SOUTH).getType() == Material.AIR;

    }

}
