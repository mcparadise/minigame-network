package net.mcparadise.deathswaps.listeners;

import me.chasertw123.minigames.core.api.minigames.MinigameUtils;
import me.chasertw123.minigames.core.api.minigames.spectating.SpectateAPI;
import me.chasertw123.minigames.shared.framework.ServerGameType;
import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.game.GameState;
import net.mcparadise.deathswaps.users.DeathSwapUser;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class Event_PlayerDeath implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {

        if (Main.getGameManager().getGameState() != GameState.GAME)
            return;

        DeathSwapUser user = Main.getUserManager().get(event.getEntity().getUniqueId());
        String playerName = user.getCoreUser().getRank().getRankColor() + user.getPlayer().getName() + ChatColor.GRAY;

        user.getCoreUser().addActivity(MinigameUtils.convertIntToOrdinal(Main.getGameManager().getAlive().size()) + " Death Swaps");
        user.setAlive(false);

        event.setDeathMessage(ChatColor.GRAY + event.getDeathMessage().replace(user.getPlayer().getName(), playerName) + "!");
        event.setKeepInventory(false);
        event.setKeepLevel(false);

        for (DeathSwapUser otherUser : Main.getGameManager().getAlive())
            if (otherUser.getCurrentSwapee() == event.getEntity().getUniqueId()
                    && (Main.getGameManager().getLastSwap() - System.currentTimeMillis()) <= 15000L) {

                String killerName = otherUser.getCoreUser().getRank().getRankColor() + otherUser.getPlayer().getName() + ChatColor.GRAY;
                event.setDeathMessage(playerName + " was swap killed by " + killerName + "!");

                //otherUser.getCoreUser().incrementStat(Stat.DEATHSWAPS_KILLS);
                otherUser.getCoreUser().giveCoinsAndExperience(ServerGameType.DEATH_SWAPS, 25, "Kill");
                break;
            }

        Location deathLocation = event.getEntity().getLocation();
        Main.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
            user.getPlayer().spigot().respawn();
            user.getPlayer().teleport(deathLocation);
        }, 2L);

        SpectateAPI.setSpectating(user.getCoreUser());

        user.getCoreUser().giveCoinsAndExperience(ServerGameType.DEATH_SWAPS, 20); // Participation
        //user.getCoreUser().incrementStat(Stat.DEATHSWAPS_PLAYTIME, (int) (System.currentTimeMillis() - Main.getGameManager().getStartTime()) / 1000);

        Main.getGameManager().checkGame();
    }

}
