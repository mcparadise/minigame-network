package net.mcparadise.deathswaps.listeners;

import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.game.GameState;
import net.mcparadise.deathswaps.game.extras.Skill;
import net.mcparadise.deathswaps.users.DeathSwapUser;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class Event_EntityDamage implements Listener {

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {

        if (Main.getGameManager().getGameState() != GameState.GAME || !(event.getEntity() instanceof Player))
            return;

        DeathSwapUser user = Main.getUserManager().get(event.getEntity().getUniqueId());
        if ((event.getCause() == EntityDamageEvent.DamageCause.FALL && user.getSkill() == Skill.FALL_PROTECTION)
                || (event.getCause() == EntityDamageEvent.DamageCause.SUFFOCATION && user.getSkill() == Skill.WATER_BREATHING))
            event.setDamage(event.getDamage() * 0.5d);

        if (Main.getGameManager().getCurrentLoop().interval <= 0)
            event.setDamage(event.getDamage() * Main.getGameManager().getDamageMultiplier());
    }
}
