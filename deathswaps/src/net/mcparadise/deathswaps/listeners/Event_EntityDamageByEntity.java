package net.mcparadise.deathswaps.listeners;

import me.chasertw123.minigames.core.api.misc.Title;
import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.game.GameState;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class Event_EntityDamageByEntity implements Listener {

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (Main.getGameManager().getGameState() != GameState.GAME
                || !(event.getEntity() instanceof Player)
                || !(event.getDamager() instanceof Player))
            return;

        if (Main.getUserManager().get(event.getDamager().getUniqueId()).isAlive()) {
            event.setCancelled(true);
            ((Player) event.getDamager()).playSound(event.getDamager().getLocation(), Sound.NOTE_BASS, 1f, 1f);
            Title.sendActionbar((Player) event.getDamager(), ChatColor.RED + "PVP is disabled. Use the swap to kill others!");
        }
    }
}
