package net.mcparadise.deathswaps.game;

import net.mcparadise.deathswaps.Main;
import org.apache.commons.io.FileUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MapManager {

    public static final int BORDER_SIZE = 500;
    public static final int BORDER_PADDING = BORDER_SIZE - 10;

    private final Random rand = new Random();
    private final World lobby, overworld, nether, end;

    private final List<Location> spawnLocations = new ArrayList<>();

    public MapManager() {
        try {
            if (!new File("lobby").exists())
                FileUtils.copyDirectory(new File("maps/lobby"), new File("lobby"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        lobby = Bukkit.getServer().createWorld(new WorldCreator("lobby"));
        lobby.setDifficulty(Difficulty.PEACEFUL);
        lobby.setGameRuleValue("doMobSpawning", "false");
        lobby.setGameRuleValue("doDaylightCycle", "false");
        lobby.setTime(6000L);

        overworld = Bukkit.getWorld("world");
        overworld.setDifficulty(Difficulty.NORMAL);
        overworld.getWorldBorder().setCenter(overworld.getSpawnLocation());
        overworld.getWorldBorder().setSize(BORDER_SIZE);

        nether = Bukkit.getWorld("world_nether");
        nether.setDifficulty(Difficulty.NORMAL);
        nether.getWorldBorder().setCenter(new Location(nether, overworld.getSpawnLocation().getX() / 8, 64, overworld.getSpawnLocation().getZ() / 8));
        nether.getWorldBorder().setSize(BORDER_SIZE);

        end = Bukkit.getWorld("world_the_end");
        end.setDifficulty(Difficulty.NORMAL);

        generateChunks();
    }

    public World getLobby() {
        return lobby;
    }

    public World getOverworld() {
        return overworld;
    }

    public World getNether() {
        return nether;
    }

    public World getEnd() {
        return end;
    }

    public List<Location> getSpawnLocations() {
        return spawnLocations;
    }

    private void generateChunks() {

        int initVal = (int) -(((this.overworld.getWorldBorder().getSize() / 2) / 16) + 12), finalVal = Math.abs(initVal);
        ChunkSnapshot spawnChunk = this.getOverworld().getSpawnLocation().getChunk().getChunkSnapshot();

        for (int x = initVal; x <= finalVal; x++)
            for (int z = initVal; z <= finalVal; z++) {

                this.getOverworld().getChunkAt(spawnChunk.getX() + x, spawnChunk.getZ() + z).load(true);
                this.getOverworld().getChunkAt(spawnChunk.getX() + x, spawnChunk.getZ() + z).unload();
            }
    }

    public void generateSpawnLocations() {

        int attempts = 0;
        while (spawnLocations.size() < 25) {
            Block block = this.getOverworld().getHighestBlockAt(Main.getMapManager().getOverworld()
                    .getSpawnLocation().clone().add(rand.nextInt(BORDER_PADDING) - (int) (BORDER_PADDING / 2f),
                            64, rand.nextInt(BORDER_PADDING) - (int) (BORDER_PADDING / 2f))).getRelative(BlockFace.DOWN);

            Material blockType = block.getType();
            if (blockType == Material.GRASS || blockType == Material.DIRT || blockType == Material.SAND || blockType == Material.SANDSTONE
                    || blockType == Material.GRAVEL || blockType == Material.STONE) {
                spawnLocations.add(block.getLocation().clone().add(0.5d, 1d, 0.5d));
                continue;
            }

            if (++attempts > 1000)
                spawnLocations.add(block.getLocation().clone().add(0.5d, 1d, 0.5d));
        }
    }
}
