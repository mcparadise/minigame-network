package net.mcparadise.deathswaps.game.loops;

import me.chasertw123.minigames.core.api.CoreAPI;
import me.chasertw123.minigames.shared.framework.GeneralServerStatus;
import net.mcparadise.deathswaps.Main;

public class Loop_ServerStatusUpdate extends GameLoop {

    public Loop_ServerStatusUpdate() {
        super(1, 20 * 15);
        CoreAPI.getServerDataManager().updateServerState(GeneralServerStatus.LOBBY, Main.MAX_PLAYERS);
    }

    @Override
    public void run() {
        CoreAPI.getServerDataManager().updateServerState(null, Main.MAX_PLAYERS);
    }
}
