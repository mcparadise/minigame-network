package net.mcparadise.deathswaps.game.loops;

import me.chasertw123.minigames.core.api.misc.Title;
import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.game.GameManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Loop_Lobby extends GameLoop {

    public Loop_Lobby() {
        super(60, 20);
    }

    @Override
    public void run() {
        Main.getMapManager().getLobby().setStorm(false);
        Main.getMapManager().getLobby().setThundering(false);

        int needed = Main.MIN_PLAYERS - Bukkit.getOnlinePlayers().size();
        if (needed > 0) {
            for (Player player : Bukkit.getOnlinePlayers())
                Title.sendActionbar(player, ChatColor.RED + "" + ChatColor.BOLD + "(!) " + ChatColor.WHITE + "Waiting for " + ChatColor.RED + needed + ChatColor.WHITE + " more player" + (needed > 1 ? "s" : ""));

            setInterval(60);
        }

        if (Main.MIN_PLAYERS <= Bukkit.getOnlinePlayers().size()) {
            if (Bukkit.getOnlinePlayers().size() >= (Main.MAX_PLAYERS * 0.75) && interval > 45) {
                setInterval(20);

                for (Player player : Bukkit.getOnlinePlayers())
                    player.sendMessage( ChatColor.RED + "Countdown reduced to 20 seconds due to lots of players!");
            }

            for (Player player : Bukkit.getOnlinePlayers())
                Title.sendActionbar(player, ChatColor.RED + "" + ChatColor.BOLD + "(!) " + ChatColor.WHITE + "Game starting in " + ChatColor.RED + interval + "s..."); // ADD 10 so that when they go into cages its seamless.

            if (interval-- <= 0) {
                Main.getGameManager().startGame();
                this.cancel();
            }
        }
    }
}
