package net.mcparadise.deathswaps.game.loops;

import me.chasertw123.minigames.core.api.misc.Title;
import me.chasertw123.minigames.shared.framework.ServerGameType;
import me.chasertw123.minigames.shared.framework.ServerSetting;
import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.game.GameManager;
import net.mcparadise.deathswaps.game.GameState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class Loop_Game extends GameLoop {

    private Loop_PreGame preGame;

    public Loop_Game() {
        super(GameManager.GAME_TIME_MINUTES * 60, 20);
        preGame = new Loop_PreGame();
    }

    @Override
    public void run() {
        if (preGame.isActive())
            return;

        Main.getGameManager().checkGame();
        if (Main.getGameManager().getGameState() != GameState.GAME)
            return;

        if (interval < (GameManager.GAME_TIME_MINUTES * 60) && interval % 45 == 0)
            Main.getGameManager().getAlive().forEach(deathSwapUser ->
                    deathSwapUser.getCoreUser().giveCoinsAndExperience(ServerGameType.DEATH_SWAPS, 4, "Playtime"));

        if (--interval <= 0)
        {
            if (interval == 0) {
                Bukkit.getServer().getOnlinePlayers().forEach(player -> {
                    player.playSound(player.getLocation(), Sound.WITHER_SPAWN, 1f, 1f);
                    Title.sendTitles(player, 0, 20, 0, ChatColor.RED + "" + ChatColor.BOLD
                            + "DEATHMATCH", ChatColor.GRAY + "Damage will continue to increase till death");
                });
            }

            else if (Math.abs(interval) % 60 == 0)
                Main.getGameManager().setDamageMultiplier(Main.getGameManager().getDamageMultiplier() * 2);
        }

        int timeTillSwap = interval - Main.getGameManager().getNextSwapTime();
        if (timeTillSwap <= 5 && timeTillSwap > 2)
            Bukkit.getServer().getOnlinePlayers().forEach(player -> {
                player.playSound(player.getLocation(), Sound.NOTE_PIANO, 1F, 0.1F);
                Title.sendTitle(player,0, 20, 0, ChatColor.GREEN + "" + timeTillSwap);
            });

        else if (timeTillSwap <= 5 && timeTillSwap > 0)
            Bukkit.getServer().getOnlinePlayers().forEach(player -> {
                player.playSound(player.getLocation(), Sound.NOTE_PIANO, 1F, (timeTillSwap == 1 ? 2f : 1f));
                Title.sendTitle(player,0, 15, 0, (timeTillSwap == 1 ? ChatColor.RED : ChatColor.YELLOW) + "" + timeTillSwap);
            });

        else if (timeTillSwap == 0)
        {
            Bukkit.getServer().getOnlinePlayers().forEach(player -> Title.sendTitles(player, 0, 5, 0, " ", " "));
            Main.getGameManager().runDeathSwaps();
        }

        if (interval <= -720) {
            Main.getGameManager().endGame(null);
        }
    }

    private static class Loop_PreGame extends GameLoop {

        private boolean active = true;

        public Loop_PreGame() {
            super(15, 20);
        }

        @Override
        public void run() {

            if (interval > 5)
                for (Player player : Bukkit.getOnlinePlayers())
                    Title.sendTitle(player,0, 20, 0, ChatColor.GREEN + "" + interval);

            else if (interval > 1)
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.playSound(player.getLocation(), Sound.NOTE_PIANO, 1F, 0.1F);
                    Title.sendTitle(player, 0, 20, 0, ChatColor.YELLOW + "" + ChatColor.BOLD + interval);
                }

            else if (interval == 1)
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.playSound(player.getLocation(), Sound.NOTE_PIANO, 1F, 2F);
                    Title.sendTitle(player, 0, 20, 0, ChatColor.RED + "" + ChatColor.BOLD + interval);
                }

            if (interval-- == 0)
            {
                Main.getGameManager().getAlive().forEach(user -> {
                    user.getCoreUser().setServerSetting(ServerSetting.DAMAGE, true);
                    user.getCoreUser().setServerSetting(ServerSetting.HUNGER, true);
                    user.getCoreUser().setServerSetting(ServerSetting.ITEM_DROPPING, true);

                    Title.sendTitle(user.getPlayer(),0, 10, 0, ChatColor.GREEN + "" + ChatColor.BOLD + "GO");
                    //Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), () -> Title.sendTitle(user.getPlayer(), 0, 1, 0, " "), 20L);

                    user.getPlayer().setWalkSpeed(0.2f);
                    user.getPlayer().setFlySpeed(0.1f);
                    user.getPlayer().setAllowFlight(false);
                    user.getPlayer().setFlying(false);
                    user.getPlayer().setGameMode(GameMode.SURVIVAL);
                    user.getPlayer().setHealth(user.getPlayer().getMaxHealth());
                    user.getPlayer().setFoodLevel(20);
                    user.getPlayer().setSaturation(14.4F);
                    user.getPlayer().playSound(user.getPlayer().getLocation(), Sound.ENDERDRAGON_GROWL, 1F, 1F);
                });

                active = false;
                this.cancel();
            }
        }

        public boolean isActive() {
            return active;
        }
    }
}
