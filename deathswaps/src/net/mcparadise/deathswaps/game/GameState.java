package net.mcparadise.deathswaps.game;

public enum GameState {
    LOBBY, STARTING, GAME, ENDING
}
