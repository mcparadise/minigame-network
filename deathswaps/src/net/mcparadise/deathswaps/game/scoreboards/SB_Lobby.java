package net.mcparadise.deathswaps.game.scoreboards;

import me.chasertw123.minigames.core.api.scoreboard.Entry;
import me.chasertw123.minigames.core.api.scoreboard.EntryBuilder;
import me.chasertw123.minigames.core.api.scoreboard.ScoreboardHandler;
import me.chasertw123.minigames.core.api.scoreboard.SimpleScoreboard;
import me.chasertw123.minigames.shared.utils.StringUtil;
import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.users.DeathSwapUser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public class SB_Lobby extends SimpleScoreboard {

    public SB_Lobby(DeathSwapUser user) {
        super(user.getPlayer());

        this.setUpdateInterval(5L);
        this.setHandler(new ScoreboardHandler() {
            @Override
            public String getTitle(Player player) {
                int time = Main.getGameManager().getCurrentLoop().interval, minutes = time / 60, seconds = time % 60;
                String mins = (minutes < 10 ? "0" : "") + minutes, secs = (seconds < 10 ? "0" : "") + seconds;

                if (Bukkit.getServer().getOnlinePlayers().size() == 1)
                    return ChatColor.WHITE + "" + ChatColor.BOLD + "Death" + ChatColor.RED + "" + ChatColor.BOLD + "Swaps"; // Don't show time

                return ChatColor.WHITE + "" + ChatColor.BOLD + "Death" + ChatColor.RED + "" + ChatColor.BOLD + "Swaps " + ChatColor.GREEN + "" + mins + ":" + secs;
            }

            @Override
            public List<Entry> getEntries(Player player) {
                EntryBuilder eb = new EntryBuilder();

                eb.blank();
                eb.next(ChatColor.BLUE + "Players: " + ChatColor.WHITE + Bukkit.getServer().getOnlinePlayers().size() + "/" + Main.MAX_PLAYERS);
                eb.blank();
                eb.next(ChatColor.GREEN + "Teleporter: " + ChatColor.WHITE + StringUtil.niceString("SOON"));
                eb.next(ChatColor.GREEN + "Skill: " + ChatColor.WHITE + user.getSkill().getDisplayName());

                return eb.build();
            }
        });

    }

}
