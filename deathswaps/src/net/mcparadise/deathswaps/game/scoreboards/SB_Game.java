package net.mcparadise.deathswaps.game.scoreboards;

import me.chasertw123.minigames.core.api.scoreboard.Entry;
import me.chasertw123.minigames.core.api.scoreboard.EntryBuilder;
import me.chasertw123.minigames.core.api.scoreboard.ScoreboardHandler;
import me.chasertw123.minigames.core.api.scoreboard.SimpleScoreboard;
import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.users.DeathSwapUser;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public class SB_Game extends SimpleScoreboard {

    public SB_Game(DeathSwapUser user)
    {
        super(user.getPlayer());

        this.setUpdateInterval(5L);
        this.setHandler(new ScoreboardHandler() {
            @Override
            public String getTitle(Player player) {
                int time = Main.getGameManager().getCurrentLoop().interval, minutes = time / 60, seconds = time % 60;
                String mins = (minutes < 10 ? "0" : "") + minutes, secs = (seconds < 10 ? "0" : "") + seconds;

                if (time <= 0)
                    return ChatColor.WHITE + "" + ChatColor.BOLD + "Death" + ChatColor.RED + "" + ChatColor.BOLD + "Swaps " + ChatColor.GREEN + "DM";

                return ChatColor.WHITE + "" + ChatColor.BOLD + "Death" + ChatColor.RED + "" + ChatColor.BOLD + "Swaps " + ChatColor.GREEN + "" + mins + ":" + secs;
            }

            @Override
            public List<Entry> getEntries(Player player) {
                EntryBuilder eb = new EntryBuilder();

                eb.blank();
                eb.next(ChatColor.YELLOW + "" + ChatColor.BOLD + "Game Info");
                eb.next(ChatColor.WHITE + "Players Left: " + ChatColor.GREEN + Main.getGameManager().getAlive().size());

                if (Main.getGameManager().getCurrentLoop().interval <= 0)
                    eb.next(ChatColor.WHITE + "Damage Multiplier: " + ChatColor.GREEN + Main.getGameManager().getDamageMultiplier() + "x");

                eb.blank();
                eb.next(ChatColor.GREEN + "" + ChatColor.BOLD + "My Info");
                eb.next(ChatColor.WHITE + "Kills: " + ChatColor.GREEN + user.getKills());
                eb.next(ChatColor.WHITE + "Swaps: " + ChatColor.GREEN + user.getSwaps());

                return eb.build();
            }
        });
    }

}
