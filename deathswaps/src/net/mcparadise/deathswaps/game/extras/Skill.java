package net.mcparadise.deathswaps.game.extras;

import me.chasertw123.minigames.core.api.misc.cItemStack;
import net.mcparadise.deathswaps.users.DeathSwapUser;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public enum Skill {

    FALL_PROTECTION("Light Footed", "You're quick with your feet. You take 50% reduced fall damage.", new ItemStack(Material.FEATHER)),
    FIRE_PROTECTION("Fire Blanket", "You always carry a fire blanket with you. You gain 8 seconds of fire protection during every swap.", new ItemStack(Material.FIREWORK_CHARGE)),
    WATER_BREATHING("Extra Lung", "You developed a third lung swimming with fish. You can breath under water and takes less damage from suffocation.", new ItemStack(Material.RAW_FISH, 1, (short) 3)),
    HASTE("Make Haste", "You have always been in a rush. You have a permanent Haste I effect buff.", new ItemStack(Material.BEACON));

    private String displayName, desc;
    private ItemStack displayItem;

    Skill(String displayName, String desc, ItemStack displayItem) {
        this.displayName = displayName;
        this.desc = desc;
        this.displayItem = displayItem;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDescription() {
        return desc;
    }

    public ItemStack toItemStack(DeathSwapUser user) {
        cItemStack item = new cItemStack(displayItem);

        if (user.getSkill() == this)
            item.addEnchant(Enchantment.LUCK, 1).addFlags(ItemFlag.HIDE_ENCHANTS);

        return item.setDisplayName(ChatColor.GREEN + this.displayName).addFancyLore(this.desc, ChatColor.GRAY.toString());
    }
}
