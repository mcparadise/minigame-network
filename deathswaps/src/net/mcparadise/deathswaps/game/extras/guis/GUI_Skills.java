package net.mcparadise.deathswaps.game.extras.guis;

import me.chasertw123.minigames.core.api.misc.AbstractGUI;
import net.mcparadise.deathswaps.game.extras.Skill;
import net.mcparadise.deathswaps.users.DeathSwapUser;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Sound;

public class GUI_Skills extends AbstractGUI {

    public GUI_Skills(DeathSwapUser user) {
        super(1, "Skill Selection", user.getCoreUser());

        int count = 0;
        for (Skill skill : Skill.values())
            setItem(skill.toItemStack(user), count++, (user1, clickType) -> {
                if (user.getSkill() == skill) {
                    user.getPlayer().playSound(user.getPlayer().getLocation(), Sound.VILLAGER_HAGGLE, 1f, 1f);
                    return;
                }

                user.setSkill(skill);

                user.getPlayer().closeInventory();
                user.getPlayer().playSound(user.getPlayer().getLocation(), Sound.LEVEL_UP, 1f, 1f);
                user.getPlayer().sendMessage(ChatColor.GRAY + "You have equipped the " + ChatColor.RED + skill.getDisplayName().toUpperCase() + ChatColor.GRAY + " skill!");
            });
    }

}
