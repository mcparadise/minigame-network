package net.mcparadise.deathswaps.game;

import me.chasertw123.minigames.core.api.minigames.MinigameUtils;
import me.chasertw123.minigames.core.api.CoreAPI;
import me.chasertw123.minigames.core.api.minigames.spectating.SpectateAPI;
import me.chasertw123.minigames.core.api.misc.cItemStack;
import me.chasertw123.minigames.shared.framework.GeneralServerStatus;
import me.chasertw123.minigames.shared.framework.ServerGameType;
import net.mcparadise.deathswaps.Main;
import net.mcparadise.deathswaps.game.extras.Skill;
import net.mcparadise.deathswaps.game.loops.GameLoop;
import net.mcparadise.deathswaps.game.loops.Loop_Game;
import net.mcparadise.deathswaps.game.loops.Loop_Lobby;
import net.mcparadise.deathswaps.game.loops.Loop_ServerStatusUpdate;
import net.mcparadise.deathswaps.game.scoreboards.SB_Game;
import net.mcparadise.deathswaps.users.DeathSwapUser;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;
import java.util.stream.Collectors;

public class GameManager {


    public static final int GAME_TIME_MINUTES = 20;
    public static final int MIN_SAFE_TIME_SECONDS = 270, MAX_SAFE_TIME_SECONDS = 315;
    public static final int MIN_SWAP_TIME_SECONDS = 75, MAX_SWAP_TIME_SECONDS = 105;

    private final Random rand = new Random();

    private GameState gameState = GameState.LOBBY;
    private GameLoop currentLoop;

    private long startTime, lastSwap;
    private int nextSwapTime = GAME_TIME_MINUTES * 60;
    private float damageMultiplier = 2;

    private boolean teleporting = false;

    public GameManager() {
        currentLoop = new Loop_Lobby();
        new Loop_ServerStatusUpdate();

        SpectateAPI.setSpectateItem(user -> user.toItemStack().setDisplayName(user.getRank().getRankColor() + user.getPlayer().getName())
                .setLore(ChatColor.WHITE + "Health: " + ChatColor.GREEN + (int) ((user.getPlayer().getHealth() / user.getPlayer().getMaxHealth()) * 100f) + "%",
                        ChatColor.WHITE + "Hunger: " + ChatColor.GREEN + (int) (((float) user.getPlayer().getFoodLevel() / 20f) * 100f) + "%",
                        ChatColor.WHITE + "Skill: " + ChatColor.GREEN + Main.getUserManager().get(user.getUUID()).getSkill().getDisplayName()));

        Main.getMapManager().generateSpawnLocations();
    }

    public void startGame() {
        this.setGameState(GameState.GAME);
        CoreAPI.getServerDataManager().updateServerState(GeneralServerStatus.INGAME, Main.MAX_PLAYERS);
        startTime = lastSwap = System.currentTimeMillis();
        nextSwapTime = (GAME_TIME_MINUTES * 60) - (rand.nextInt(MAX_SAFE_TIME_SECONDS - MIN_SAFE_TIME_SECONDS) + MIN_SAFE_TIME_SECONDS);
        teleporting = true;

        Main.getMapManager().getOverworld().setTime(6000L);

        List<Location> spawnLocs = Main.getMapManager().getSpawnLocations();
        Main.getUserManager().toCollection().forEach(user -> {

            Location spawn = spawnLocs.remove(0);
            for (int x = spawn.getChunk().getX() - 5; x <= 5; x++)
                for (int z = spawn.getChunk().getZ() - 5; z <= 5; z++)
                    spawn.getWorld().getChunkAt(spawn.getChunk().getX() + x, spawn.getChunk().getX() + z);

            if (spawn.getBlock().getRelative(BlockFace.DOWN).isLiquid())
                spawn.getBlock().getRelative(BlockFace.DOWN).setType(Material.STONE);

            user.getCoreUser().setScoreboard(new SB_Game(user));

            user.getPlayer().getInventory().clear();
            user.getPlayer().setWalkSpeed(0f);
            user.getPlayer().setFlySpeed(0f);
            user.getPlayer().setAllowFlight(true);
            user.getPlayer().setFlying(true);
            user.getPlayer().teleport(spawn);

            if (user.getSkill() == Skill.HASTE)
                user.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 0, false, false));

            else if (user.getSkill() == Skill.WATER_BREATHING)
                user.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, Integer.MAX_VALUE, 0, false, false));
        });

        currentLoop = new Loop_Game();
        teleporting = false;
    }

    public void checkGame() {
        List<DeathSwapUser> alive = this.getAlive();

        if (alive.size() == 1)
            endGame(alive.get(0));

        else if (getAlive().size() == 0)
            endGame(null);
    }

    public void endGame(DeathSwapUser winner) {
        this.setGameState(GameState.ENDING);
        currentLoop.cancel();

        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
            CoreAPI.getServerDataManager().updateServerState(GeneralServerStatus.RESTARTING, Main.MAX_PLAYERS);
            for (Player player : Bukkit.getServer().getOnlinePlayers())
                CoreAPI.getUser(player).sendToServer("hub");
        }, 20 * 15);

        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
            CoreAPI.getServerDataManager().updateServerState(GeneralServerStatus.DELETE, Main.MAX_PLAYERS);
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "stop");
        }, 20 * 25);

        SpectateAPI.setCanUseTeleporter(false);
        Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.ENDERDRAGON_GROWL, 0.75F, 1F));

        try {
        if (winner != null) {
            winner.getCoreUser().addActivity("1st Death Swaps");
//        winner.getCoreUser().incrementStat(Stat.DEATHSWAPS_PLAYTIME, (int) (System.currentTimeMillis() - startTime) / 1000);
//        winner.getCoreUser().incrementStat(Stat.DEATHSWAPS_WON);
            winner.getCoreUser().giveCoinsAndExperience(ServerGameType.DEATH_SWAPS, 50);
            winner.setAlive(false);

            SpectateAPI.setSpectating(winner.getCoreUser());
        }


            MinigameUtils.Messages.sendGameOverMessage(winner == null ? null : winner.getCoreUser());
        } catch (Exception e) {
            if (Bukkit.getPlayer("chasertw123") != null)
                Bukkit.getPlayer("chasertw123").sendMessage(e.getCause().getMessage());
        }

        MinigameUtils.showAllPlayers();
        Bukkit.getServer().getScheduler().runTaskTimer(Main.getInstance(), MinigameUtils::playFireworks, 0, 20);
    }

    private static class UUIDLocationMap {

        private final UUID uuid;
        private final Location location;

        public UUIDLocationMap(UUID uuid, Location location) {
            this.uuid = uuid;
            this.location = location;
        }

        public UUID getUuid() {
            return uuid;
        }

        public Location getLocation() {
            return location;
        }

        public Player getPlayer() {
            return Bukkit.getPlayer(this.getUuid());
        }
    }

    public void runDeathSwaps() {
        nextSwapTime -= (rand.nextInt(MAX_SWAP_TIME_SECONDS - MIN_SWAP_TIME_SECONDS) + MIN_SWAP_TIME_SECONDS);
        lastSwap = System.currentTimeMillis();

        teleporting = true;

        List<DeathSwapUser> alive = this.getAlive();
        List<UUIDLocationMap> users = this.getAlive().stream().map(user -> new UUIDLocationMap(user.getPlayer().getUniqueId(),
                user.getPlayer().getLocation().clone())).collect(Collectors.toList());
        Collections.shuffle(users);
        Collections.shuffle(alive);

        int usersLeft = alive.size();
        while(usersLeft != 0) {
            if(usersLeft == 3) {
                // This is the edge case
                usersLeft = 0;

                // We need to basically shift all three people along one so like:
                // A -> B -> C -> [A]

                // All we need to add is a bit more logic that goes A can't have B or C's location.
                // As such, B can't start with C's (A is impossible)
                // C physcially can't have any of the others.

                DeathSwapUser a = alive.remove(0);
                DeathSwapUser b = alive.remove(0);
                DeathSwapUser c = alive.remove(0);

                UUIDLocationMap aLoc = attemptTargetFind(users, a.getPlayer().getUniqueId(), b.getPlayer().getUniqueId(), c.getPlayer().getUniqueId());
                UUIDLocationMap bLoc = attemptTargetFind(users, b.getPlayer().getUniqueId(), c.getPlayer().getUniqueId());
                UUIDLocationMap cLoc = attemptTargetFind(users, c.getPlayer().getUniqueId());

//                Location aClone = a.getPlayer().getLocation().clone();

                a.getPlayer().teleport(bLoc.getLocation().clone());
                Main.getUserManager().get(bLoc.getUuid()).setCurrentSwapee(a.getPlayer().getUniqueId());
                this.sendTargetFeedback(a.getPlayer(), bLoc.getPlayer().getUniqueId());

                b.getPlayer().teleport(cLoc.getLocation().clone());
                Main.getUserManager().get(cLoc.getUuid()).setCurrentSwapee(b.getPlayer().getUniqueId());
                this.sendTargetFeedback(b.getPlayer(), cLoc.getPlayer().getUniqueId());

                c.getPlayer().teleport(aLoc.getLocation().clone());
                a.setCurrentSwapee(c.getPlayer().getUniqueId());
                this.sendTargetFeedback(c.getPlayer(), aLoc.getPlayer().getUniqueId());

                break;
            }

            // Get this
            DeathSwapUser selected = alive.remove(--usersLeft);

            // Handle ONE next user
            UUIDLocationMap target = users.remove(0);

            if(selected.getPlayer().getUniqueId() == target.getUuid()) {
                users.add(target);

                target = users.remove(0);
            }

            Main.getUserManager().get(target.getUuid()).setCurrentSwapee(selected.getPlayer().getUniqueId());
            selected.getPlayer().teleport(target.getLocation());

            this.sendTargetFeedback(selected.getPlayer(), target.getUuid());
        }

        teleporting = false;
    }

    private UUIDLocationMap attemptTargetFind(List<UUIDLocationMap> mapList, UUID toCheck, UUID... cantBe) {
        Optional<UUIDLocationMap> found = mapList.stream().filter(item -> item.getUuid() == toCheck).findFirst();

        if(found.isPresent()) {
            UUIDLocationMap foundItem = found.get();

            // We need to remove this item
            mapList.remove(foundItem);

            return foundItem;
        }

        List<UUID> converted = new ArrayList<>();
        Collections.addAll(converted, cantBe);
        return mapList.stream().filter(item -> !converted.contains(item.getUuid())).findFirst().get();
    }

    private void sendTargetFeedback(Player selected, UUID target) {
        selected.playSound(selected.getPlayer().getLocation(), Sound.WITHER_SPAWN, 1f, 1f);
        selected.sendMessage(ChatColor.GRAY + "You swapped to " + ChatColor.RED + Bukkit.getPlayer(target)
                .getPlayer().getName() + ChatColor.GRAY + "!");

        DeathSwapUser user = Main.getUserManager().get(selected.getUniqueId());

        if (user.getSkill() == Skill.FIRE_PROTECTION && user.getPlayer().getActivePotionEffects().stream()
            .noneMatch(potionEffect -> potionEffect.getType() == PotionEffectType.FIRE_RESISTANCE && potionEffect.getDuration() > 160))
            user.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 160, 1, true, true));
    }

    public List<DeathSwapUser> getAlive() {
        return Main.getUserManager().toCollection().stream().filter(DeathSwapUser::isAlive).collect(Collectors.toList());
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public long getStartTime() {
        return startTime;
    }

    public GameLoop getCurrentLoop() {
        return currentLoop;
    }

    public int getNextSwapTime() {
        return nextSwapTime;
    }

    public long getLastSwap() {
        return lastSwap;
    }

    public float getDamageMultiplier() {
        return damageMultiplier;
    }

    public void setDamageMultiplier(float damageMultiplier) {
        this.damageMultiplier = damageMultiplier;
    }

    public boolean isTeleporting() {
        return teleporting;
    }

    public boolean canTower() {
        return startTime < lastSwap;
    }
}
