package net.mcparadise.deathswaps.users;

import me.chasertw123.minigames.core.api.CoreAPI;
import me.chasertw123.minigames.core.user.User;
import net.mcparadise.deathswaps.game.extras.Skill;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class DeathSwapUser {

    private UUID uuid, currentSwapee;
    private boolean isAlive = true;

    private Skill skill = Skill.FALL_PROTECTION;

    private int kills, swaps;

    public DeathSwapUser(UUID uuid) {
        this.uuid = uuid;
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(uuid);
    }

    public User getCoreUser() {
        return CoreAPI.getUser(this.getPlayer());
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        this.isAlive = alive;
    }

    public UUID getCurrentSwapee() {
        return currentSwapee;
    }

    public void setCurrentSwapee(UUID currentSwapee) {
        this.currentSwapee = currentSwapee;
    }

    public int getKills() {
        return kills;
    }

    public void incrementKills() {
        kills++;
    }

    public int getSwaps() {
        return swaps;
    }

    public void incrementSwaps() {
        swaps++;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }
}
