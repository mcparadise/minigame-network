package net.mcparadise.deathswaps.users;

import java.util.*;

public class UserManager {

    private Map<UUID, DeathSwapUser> users = new HashMap<>();

    public DeathSwapUser get(UUID uuid) {
        return users.get(uuid);
    }

    public boolean has(UUID uuid) {
        return users.containsKey(uuid);
    }

    public void add(DeathSwapUser user) {
        users.put(user.getPlayer().getUniqueId(), user);
    }

    public Collection<DeathSwapUser> toCollection(){
        return users.values();
    }

    public void remove(UUID uuid){
        users.remove(uuid);
    }
}
