package me.chasertw123.minigames.hub.listeners;

import me.chasertw123.minigames.core.api.misc.AbstractItem;
import me.chasertw123.minigames.core.api.misc.Item;
import me.chasertw123.minigames.core.api.misc.Title;
import me.chasertw123.minigames.core.api.CoreAPI;
import me.chasertw123.minigames.core.user.data.settings.Setting;
import me.chasertw123.minigames.core.user.data.stats.Stat;
import me.chasertw123.minigames.core.api.misc.cItemStack;
import me.chasertw123.minigames.hub.Main;
import me.chasertw123.minigames.hub.features.bossbar.Loop_AnimatedBossBar;
import me.chasertw123.minigames.hub.features.guis.collectibles.GUI_Collectibles;
import me.chasertw123.minigames.hub.features.scoreboards.SB_Hub;
import me.chasertw123.minigames.hub.user.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 * Created by Chase on 1/6/2018.
 */
public class Event_PlayerJoin implements Listener {

    AbstractItem collectibles =  new AbstractItem(new cItemStack(Material.CHEST, ChatColor.GREEN + "Arcade Prizes")
            .addFancyLore("Feeling lucky? Why don't you open some treasure chests!", ChatColor.WHITE.toString()),
            (user, clickType) -> new GUI_Collectibles(Main.getUserManager().get(user.getUUID())));

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoinEvent(PlayerJoinEvent event) {

        User user = new User(event.getPlayer().getUniqueId());
        me.chasertw123.minigames.core.user.User pp = user.getCoreUser();

        Main.getUserManager().add(user);

        if (Loop_AnimatedBossBar.getInstance().getBossBar() != null && pp.getSetting(Setting.BOSS_BAR))
            Loop_AnimatedBossBar.getInstance().getBossBar().addPlayer(pp.getUUID());

        pp.getPlayer().getInventory().clear();
        pp.getPlayer().getEquipment().clear();

        for (PotionEffect potionEffect : pp.getPlayer().getActivePotionEffects())
            pp.getPlayer().removePotionEffect(potionEffect.getType());

        if (!pp.isDeluxe())
            pp.setSetting(Setting.SCOREBOARD, true);

        if (pp.getSetting(Setting.SCOREBOARD))
            pp.setScoreboard(new SB_Hub(user));

        pp.getPlayer().getInventory().setHeldItemSlot(4);
        pp.getPlayer().setAllowFlight(pp.getSetting(Setting.AUTO_FLY) && pp.isDeluxe());
        pp.getPlayer().setGameMode(CoreAPI.getServerDataManager().getDefaultGamemode());
        pp.getPlayer().teleport(Main.getSpawnpointManager().getHubSpawnpoint());
//        pp.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false));
        pp.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0, false, false));

//        float xp = ((float) pp.getStat(Stat.EXPERIENCE) / pp.getLevelExperience());
        float xp = ((float) (pp.getStat(Stat.EXPERIENCE) % 250)) / 250.f;
        pp.getPlayer().setExp(xp >= 1 ? 0.9999999F : xp);
        pp.getPlayer().setLevel(pp.getLevel());

        net.md_5.bungee.api.ChatColor nameColor = pp.getRank().getRankColor();
        if (!pp.getRank().isStaff() && pp.isDeluxe())
            nameColor = net.md_5.bungee.api.ChatColor.GOLD;

        pp.getPlayer().setPlayerListName(nameColor + pp.getUsername());
        event.setJoinMessage(pp.getRank().isStaff() || pp.isDeluxe() ? user.getSelectedLoginMessage().createMessage(pp) : null);
        Title.sendTablist(pp.getPlayer(), ChatColor.GOLD + "" + ChatColor.BOLD + "YANA", ChatColor.WHITE + "YANA.gg");

        setPlayerNick(pp);

        // TODO: FIX THIS CAUSE IT WILL CAUSE A MEMORY LEAK
        new AbstractItem(new cItemStack(pp.getPlayer().getName(), ChatColor.GREEN + "My Profile").addFancyLore("View all of your statistics," +
                "achievements, and more!", ChatColor.WHITE.toString()), (user1, clickType) -> user1.getPlayer().performCommand("profile")).give(pp.getPlayer(), 0);

        collectibles.give(pp.getPlayer(), 1);
        Item.QUICK_PLAY.give(pp.getPlayer(), 4);

        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), user::updatePlayerVisibility, 3L);
        CoreAPI.getOnlinePlayers().stream().filter(paradisePlayer -> !paradisePlayer.getUUID().equals(pp.getUUID()))
                .filter(paradisePlayer -> paradisePlayer.getSetting(Setting.PLAYER_VISIBILITY)).forEach(paradisePlayer -> paradisePlayer.getPlayer().hidePlayer(pp.getPlayer()));

        if (pp.hasLeveled())
            Main.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
                pp.setLeveled(false);
                pp.getPlayer().playSound(pp.getPlayer().getPlayer().getLocation(), Sound.LEVEL_UP, 1.25F, 0.5F);
                pp.sendPrefixedMessage(ChatColor.YELLOW + "You leveled up! You're now level " + ChatColor.LIGHT_PURPLE + pp.getLevel() + ChatColor.YELLOW + "!");
            }, 10);
    }

    private void setPlayerNick(me.chasertw123.minigames.core.user.User pp)
    {
        if (pp.getRank().isStaff())
            setNick(pp.getPlayer(), pp.getRank().getRankColor() + "" + ChatColor.BOLD + "[" + pp.getRank().toString().toUpperCase() + "] " + ChatColor.RESET, " " + ChatColor.GOLD + "" + ChatColor.BOLD + "[YANA]");

        else
            setNick(pp.getPlayer(), (pp.isDeluxe() ? ChatColor.GOLD : ChatColor.GRAY) + "", "");

        Main.getUserManager().toCollection().forEach(user -> {
            if (user.getCoreUser().getRank().isStaff())
                setNick(user.getCoreUser().getPlayer(), user.getCoreUser().getRank().getRankColor()  + "" + ChatColor.BOLD + "[" + pp.getRank().toString().toUpperCase() + "] " + ChatColor.RESET, " " + ChatColor.GOLD + "" + ChatColor.BOLD + "[YANA]");

            else
                setNick(user.getCoreUser().getPlayer(), (user.getCoreUser().isDeluxe() ? ChatColor.GOLD : ChatColor.GRAY) + "", "");
        });
    }

    private void setNick(Player player, String prefix, String suffix) {
        for (Player online : Bukkit.getOnlinePlayers()) {
            if (!online.canSee(player))
                continue;

            Scoreboard scoreboard = online.getScoreboard();
            if (scoreboard == Bukkit.getScoreboardManager().getMainScoreboard())
                scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

            Team team = scoreboard.getTeam(player.getName());
            if (team == null)
                team = scoreboard.registerNewTeam(player.getName());

            team.setPrefix(prefix);
            team.setSuffix(suffix);
            team.addEntry(player.getName());
            online.setScoreboard(scoreboard);
        }

        for (Player online : Bukkit.getOnlinePlayers()) {
            if (!online.canSee(player))
                continue;

            if (online.getUniqueId().equals(player.getUniqueId()))
                continue;

            Scoreboard scoreboard = player.getScoreboard();
            if (scoreboard == Bukkit.getScoreboardManager().getMainScoreboard())
                scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

            Team team = scoreboard.getTeam(online.getName());
            if (team == null)
                team = scoreboard.registerNewTeam(online.getName());

            team.setPrefix(player.getScoreboard().getTeam(online.getName()).getPrefix());
            team.setSuffix(player.getScoreboard().getTeam(online.getName()).getSuffix());
            team.addEntry(online.getName());
            player.setScoreboard(scoreboard);
        }
    }

}
