package me.chasertw123.minigames.hub.features.shops.waterwars;

import me.chasertw123.minigames.core.api.misc.AbstractGUI;
import me.chasertw123.minigames.core.api.misc.cItemStack;
import me.chasertw123.minigames.hub.features.shops.ShopCategory;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Created by Chase on 1/13/2018.
 */
public class Category_ArrowTrails extends ShopCategory {

    public Category_ArrowTrails() {
        super("Arrow Trails", new cItemStack(Material.ARROW), Sound.SHOOT_ARROW);
    }

    @Override
    public void buildShopCategory(Player player, AbstractGUI abstractGui) {

    }
}
