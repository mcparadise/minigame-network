package me.chasertw123.minigames.bungee.events;

import me.chasertw123.minigames.bungee.Main;
import me.chasertw123.minigames.bungee.packets.ParadisePacket;
import me.chasertw123.minigames.bungee.user.User;
import me.chasertw123.minigames.shared.database.Database;
import me.chasertw123.minigames.shared.framework.ServerGameType;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import org.bson.Document;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chase on 7/1/2017.
 */
public class BungeeEvent_PostLogin implements Listener {

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {
        ProxiedPlayer player = e.getPlayer();

        Main.getInstance().getUserManager().add(player);

        // See if a ban exists
        Document query = new Document();
        query.put("uuid", player.getUniqueId() + "");
        query.put("banned", true);
        Document doc = Main.getInstance().getDatabase().getMongoCollection(Database.Collection.GBANS).find(query).first();

        if(doc != null) {
            player.disconnect(ChatColor.RED + "You are banned!");

            return;
        }

        List<ParadisePacket> toRemove = new ArrayList<>();
        for(ParadisePacket packet : Main.getInstance().getParadisePacketManager().getPacketUserQueue()){
            if(packet.getUsername() != null && packet.getUsername().equalsIgnoreCase(e.getPlayer().getName())) {
                //Send the packet
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                DataOutputStream out = new DataOutputStream(stream);

                try {
                    out.writeUTF(packet.getSubChannel());
                    for(String s : packet.getData())
                        out.writeUTF(s);
                } catch (IOException ev) {
                    ev.printStackTrace();
                }

                BungeeCord.getInstance().getServerInfo(e.getPlayer().getServer().getInfo().getName()).sendData(packet.getChannel(), stream.toByteArray());

                toRemove.add(packet);
            }
        }

        for(ParadisePacket packet : toRemove)
            Main.getInstance().getParadisePacketManager().deleteUserPacket(packet);
    }

}
