package me.chasertw123.minigames.bungee.loops;

import com.mongodb.Block;
import com.mongodb.client.model.Filters;
import me.chasertw123.minigames.bungee.Main;
import me.chasertw123.minigames.shared.database.Database;
import net.md_5.bungee.BungeeCord;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Loop_PanelBungeeExecutor {

    public Loop_PanelBungeeExecutor() {
        BungeeCord.getInstance().getScheduler().schedule(Main.getInstance(), () -> {
            // Check for messages
            Document query = new Document();
            query.put("target", "bungee");
            query.put("completed", false);

            Main.getInstance().getDatabase().getMongoCollection(Database.Collection.PANEL_MESSAGES)
                    .find(query).forEach((Block<? super Document>) record -> {

                if(record.getBoolean("completed"))
                    return;

                String command = record.getString("command");
                BungeeCord.getInstance().getPluginManager().dispatchCommand(BungeeCord.getInstance().getConsole(), command);

                record.put("completed", true);

                // Save the record
                Main.getInstance().getDatabase().getMongoCollection(Database.Collection.PANEL_MESSAGES)
                        .deleteOne(Filters.eq("uuid", record.getString("uuid")));
            });
        }, 0, 5, TimeUnit.SECONDS);
    }
}
