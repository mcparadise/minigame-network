package me.chasertw123.minigames.bungee.loops;

import com.mongodb.client.model.Filters;
import me.chasertw123.minigames.bungee.Main;
import me.chasertw123.minigames.bungee.queue.v2.QueueGroup;
import me.chasertw123.minigames.bungee.queue.v2.ServerInstance;
import me.chasertw123.minigames.shared.database.Database;
import me.chasertw123.minigames.shared.framework.ServerGameType;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.config.ServerInfo;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Scott Hiett on 8/1/2017.
 */
public class Loop_QueueCheck {

    /**
     * Updates all of the GameQueues to check for available matches.
     */
    public Loop_QueueCheck(){
        BungeeCord.getInstance().getScheduler().schedule(Main.getInstance(), () -> {
            // New Queue Controller!
//            Main.getInstance().getQueueController().processQueue();

            Main.getInstance().getQueueManager().refreshServerCache();

            // Update the data for the player counts in queue and games to the server.
            Arrays.asList(ServerGameType.values()).forEach(serverType -> {
                Document data = new Document();

                int count = 0;

                for(ServerInfo serverInfo : BungeeCord.getInstance().getServers().values()) {
                    ServerGameType gameType = Main.getInstance().getQueueManager().getCurrentServerTypeMappings().get(serverInfo.getName());

                    if(gameType == null || gameType != serverType)
                        continue;

                    count += serverInfo.getPlayers().size();
                }

//                for(ServerInstance inst : Main.getInstance().getQueueManager().getServers())
//                    if(inst.getGameType() == serverType)
//                        count += BungeeCord.getInstance().getServerInfo(inst.getId()).getPlayers().size();

//                for(QueueGroup queueGroup : Main.getInstance().getQueueController().getQueueLoads())
//                    if(queueGroup.getType() == serverType)
//                        count += queueGroup.getPlayers().size();

                data.put("serverType", serverType.toString());
                data.put("playerCount", count);

                Main.getInstance().getDatabase().getMongoCollection(Database.Collection.PLAYER_COUNTS)
                        .replaceOne(Filters.eq("serverType", serverType.toString()), data, Database.upsert());
            });

            // Hub count
            Document hubDoc = new Document();
            hubDoc.put("serverType", "HUB");
            hubDoc.put("playerCount", BungeeCord.getInstance().getServerInfo("hub").getPlayers().size());

            Main.getInstance().getDatabase().getMongoCollection(Database.Collection.PLAYER_COUNTS)
                    .replaceOne(Filters.eq("serverType", "HUB"), hubDoc, Database.upsert());
        }, 0, 5, TimeUnit.SECONDS);
    }

}
