package me.chasertw123.minigames.bungee.loops;

import me.chasertw123.minigames.bungee.Main;
import me.chasertw123.minigames.bungee.messages.DiscordMessage;
import me.chasertw123.minigames.bungee.messages.DonateMessage;
import me.chasertw123.minigames.bungee.messages.ReportMessage;
import me.chasertw123.minigames.bungee.user.User;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.concurrent.TimeUnit;

public class Loop_AutoMessage {

    private static final Message[] MESSAGES = new Message[]{new ReportMessage(), new DonateMessage(), new DiscordMessage()};
    private static int CURRENT_MESSAGE = 0;

    public Loop_AutoMessage() {
        BungeeCord.getInstance().getScheduler().schedule(Main.getInstance(), () -> {
            Message message = MESSAGES[CURRENT_MESSAGE];

            for(ProxiedPlayer player : BungeeCord.getInstance().getPlayers()) {
                User user = User.get(player);

                message.sendMessage(user); // Message can contain like personalisation or something
            }

            if(++CURRENT_MESSAGE == MESSAGES.length)
                CURRENT_MESSAGE = 0;
        }, 0, 5, TimeUnit.MINUTES);
    }

    public interface Message {

        void sendMessage(User user);
    }
}
