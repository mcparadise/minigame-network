package me.chasertw123.minigames.bungee.queue.v3;

import me.chasertw123.minigames.bungee.Main;
import me.chasertw123.minigames.bungee.queue.v2.ServerInstance;
import me.chasertw123.minigames.bungee.queue.v2.ServerState;
import me.chasertw123.minigames.bungee.user.User;
import me.chasertw123.minigames.shared.database.Database;
import me.chasertw123.minigames.shared.framework.ServerGameType;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import org.bson.Document;

import java.net.InetSocketAddress;
import java.util.*;

public class QueueManager {

    private List<ServerInstance> servers;
    private Map<String, ServerGameType> currentServerTypeMappings = new HashMap<>();

    public QueueManager() {
        this.servers = new ArrayList<>();
    }

    public void refreshServerCache() {
        // Check the mongo for all active servers.
        this.servers = new ArrayList<>();

        for(Document d : Main.getInstance().getDatabase().getMongoCollection(Database.Collection.SERVER_STATUS).find()) {
            // Find the server
//            boolean contains = false;

            ServerGameType serverGameType = ServerGameType.find(d.getString("gametype"));
            if(serverGameType == null)
                serverGameType = ServerGameType.WATER_WARS;

            ServerState state = ServerState.Companion.fromId(d.getInteger("status"));
            String serverName = d.getString("servername");

            ServerInstance instance = new ServerInstance(serverName,
                    state,
                    serverGameType,
                    d.getInteger("currentplayers"),
                    d.getInteger("maxplayers"));

            // Check if this is registered in the proxy
            ServerInfo info = ProxyServer.getInstance().constructServerInfo(instance.getId(), new InetSocketAddress(instance.getIp(), 25565),
                    instance.getId(), false);

//            for(ServerInstance instance : servers.stream().filter(item -> item.getName().equalsIgnoreCase(d.getString("servername")))
//                    .collect(Collectors.toList())) {
//
//                // We need to update this instance
//
//                contains = true;
//            }

            if(state == ServerState.LOBBY || state == ServerState.INGAME)
                currentServerTypeMappings.put(instance.getId(), serverGameType);

            if((state == ServerState.RESTARTING || state == ServerState.DELETE) && currentServerTypeMappings.containsKey(instance.getId()))
                currentServerTypeMappings.remove(instance.getId());

            if(state == ServerState.LOBBY) {
                // In this case, we need to add this new server
                this.servers.add(instance);

                if(!ProxyServer.getInstance().getServers().containsKey(instance.getId())) {
                    ProxyServer.getInstance().getServers().put(instance.getId(), info); // Add the server.

                    System.out.println("Adding server " + instance.getId() + " / " + instance.getIp());
                }
            } else if(state != ServerState.INGAME
                    && ProxyServer.getInstance().getServers().containsKey(instance.getId())) {

                ProxyServer.getInstance().getServers().remove(instance.getId());
            }
        }
    }

    public Map<String, ServerGameType> getCurrentServerTypeMappings() {
        return currentServerTypeMappings;
    }

    public void processThroughQueue(User user, ServerGameType gameType) {
        // Find out if any of these servers is of the correct gamemode and state
        Optional<ServerInstance> server = servers.stream()
                .filter(serverInstance -> serverInstance.getGameType() == gameType
                        && serverInstance.getState() == ServerState.LOBBY
                        && serverInstance.getPlayerCount() < serverInstance.getMaxPlayers())
                .findFirst();

        if(!server.isPresent()) {
            user.getPlayer().sendMessage(ChatColor.GOLD + "Sadly, there are no lobbies available. Please try again soon!");

            return;
        }

        // Put this person in the server & update the server count
        ServerInstance instance = server.get();

        user.getPlayer().connect(BungeeCord.getInstance().getServerInfo(instance.getId()));
        instance.setPlayerCount(instance.getPlayerCount() + 1);
    }

    public List<ServerInstance> getServers() {
        return servers;
    }
}
