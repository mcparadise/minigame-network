package me.chasertw123.minigames.bungee.queue.v2

import me.chasertw123.minigames.shared.framework.ServerGameType
import kotlin.math.max
import kotlin.math.min

data class ServerInstance(val name: String, var state: ServerState, val gameType: ServerGameType, var playerCount: Int, var maxPlayers: Int) {

    fun getIp(): String {
        val parts: Array<String> = name.split("server-".toRegex()).toTypedArray()
        val ipStart = parts[1]
        return ipStart.split("-".toRegex()).toTypedArray()[0]
    }

    fun getId(): String {
        val cleaned = name.replace("-", "").replace(".", "")

        return cleaned.substring(max(0, cleaned.length - 8), cleaned.length);
    }
}