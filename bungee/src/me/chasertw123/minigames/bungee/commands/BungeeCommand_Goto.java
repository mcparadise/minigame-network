package me.chasertw123.minigames.bungee.commands;

import me.chasertw123.minigames.bungee.user.User;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BungeeCommand_Goto extends BungeeCommand {

    public BungeeCommand_Goto() {
        super("goto");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if(!(commandSender instanceof ProxiedPlayer)) {
            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) commandSender;
        User user = User.get(player);

        if(!user.getRank().isStaff()) {
            player.sendMessage(ChatColor.RED + "You can't do that!");

            return;
        }

        if(args.length != 1) {
            player.sendMessage("Incorrect args. /goto <username>");

            return;
        }

        ProxiedPlayer target = BungeeCord.getInstance().getPlayer(args[0]);

        if(target == null) {
            player.sendMessage("Cannot find that player.");

            return;
        }

        player.sendMessage(ChatColor.GOLD + "Sending you to " + target.getName() + "!");
        player.connect(target.getServer().getInfo());
    }
}
