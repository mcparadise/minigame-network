package me.chasertw123.minigames.bungee.commands;

import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import me.chasertw123.minigames.bungee.Main;
import me.chasertw123.minigames.bungee.user.User;
import me.chasertw123.minigames.shared.database.Database;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bson.Document;

import java.util.UUID;

public class BungeeCommand_GBan extends BungeeCommand {

    public BungeeCommand_GBan() {
        super("gban");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if(commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;
            User user = User.get(player);

            if(!user.getRank().isStaff()) {
                commandSender.sendMessage(ChatColor.RED + "You're not allowed to do that command!");

                return;
            }
        }

        if(args.length != 1) {
            commandSender.sendMessage(ChatColor.RED + "Incorrect args: /gban <username>");

            return;
        }

        ProxiedPlayer otherPlayer = BungeeCord.getInstance().getPlayer(args[0]);
        String toBanName;
        String toBanUUID;

        if(otherPlayer == null) {
            // Search the database
            MongoCursor<Document> potentialUser = Main.getInstance().getDatabase().getMongoCollection(Database.Collection.USERS).find(Filters.eq("lastknownusername", args[0])).iterator();

            if(!potentialUser.hasNext()) {
                commandSender.sendMessage(ChatColor.RED + "Cannot find player " + args[0] + ".");

                return;
            }

            toBanName = potentialUser.next().getString("lastknownusername");
            toBanUUID = potentialUser.next().getString("uuid");
        } else {
            toBanName = otherPlayer.getName();
            toBanUUID = otherPlayer.getUniqueId() + "";
        }

        Document banDocument = new Document();
        banDocument.put("uuid", toBanUUID);
        banDocument.put("username", toBanName);
        banDocument.put("banned", true);
        banDocument.put("staff", commandSender.getName());

        Main.getInstance().getDatabase().getMongoCollection(Database.Collection.GBANS).replaceOne(Filters.eq("uuid", toBanUUID), banDocument, Database.upsert());

        if(otherPlayer != null) {
            otherPlayer.disconnect(ChatColor.RED + "You have been banned!");
        }

        commandSender.sendMessage(ChatColor.RED + "Banned " + toBanName + ".");
    }
}
