package me.chasertw123.minigames.bungee.messages;

import me.chasertw123.minigames.bungee.loops.Loop_AutoMessage;
import me.chasertw123.minigames.bungee.user.User;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class ReportMessage implements Loop_AutoMessage.Message {

    @Override
    public void sendMessage(User user) {
        BaseComponent[] message = new ComponentBuilder("YANA").color(ChatColor.GOLD).bold(true).append(" > ")
                .color(ChatColor.DARK_GRAY)
                .append("Someone doing something they shouldn't be? Report them to staff with ")
                .color(ChatColor.WHITE)
                .bold(false)
                .append("/report <username> <reason>")
                .color(ChatColor.RED)
                .append("!")
                .color(ChatColor.WHITE)
                .bold(false)
                .create();

        user.getPlayer().sendMessage(message);
    }
}
