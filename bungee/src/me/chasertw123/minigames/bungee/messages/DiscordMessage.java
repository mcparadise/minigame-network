package me.chasertw123.minigames.bungee.messages;

import me.chasertw123.minigames.bungee.loops.Loop_AutoMessage;
import me.chasertw123.minigames.bungee.user.User;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class DiscordMessage implements Loop_AutoMessage.Message {

    @Override
    public void sendMessage(User user) {
        BaseComponent[] message = new ComponentBuilder("YANA").color(ChatColor.GOLD).bold(true).append(" > ")
                .color(ChatColor.DARK_GRAY)
                .append("Hey " + user.getPlayer().getName() + ", why not join our Discord server? Click here to do so!")
                .color(ChatColor.WHITE)
                .bold(false)
                .event(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://discord.gg/r4sMdgg")).create();

        user.getPlayer().sendMessage(message);
    }
}
