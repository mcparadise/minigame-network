package me.chasertw123.minigames.bungee.messages;

import me.chasertw123.minigames.bungee.loops.Loop_AutoMessage;
import me.chasertw123.minigames.bungee.user.User;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class DonateMessage implements Loop_AutoMessage.Message {

    @Override
    public void sendMessage(User user) {
        BaseComponent[] message = new ComponentBuilder("YANA").color(ChatColor.GOLD).bold(true).append(" > ")
                .color(ChatColor.DARK_GRAY).append("Hey " + user.getPlayer().getName() + ", click here to donate to YANA-supported charities!")
                .color(ChatColor.WHITE)
                .bold(false).event(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://yana.gg/donate")).create();

        user.getPlayer().sendMessage(message);
    }
}
