#!/bin/bash

echo "Starting Proxy Server and Holding Server"

# turn on bash's job control
set -m

# Start the primary process and put it in the background
java -jar bungee.jar &

# Start the helper process
cd containedserver&&java -jar spigot.jar

# now we bring the primary process back into the foreground
# and leave it there
fg %1