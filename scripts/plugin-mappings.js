const PLUGIN_MAPPINGS = {
  waterwars: ["core"],
  deathswaps: ["core"],
  hub: ["core"],
  splegg: ["core"],
};

module.exports = PLUGIN_MAPPINGS;