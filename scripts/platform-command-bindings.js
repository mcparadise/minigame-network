const isWin = process.platform === "win32";

const PLATFORM_COMMAND_BINDINGS = {
  "ls": {unix: "ls", win: "dir"},
  "cp": {unix: "cp", win: "copy"},
};

const getCommandForSystem = command => {
  const target = PLATFORM_COMMAND_BINDINGS[command];

  return isWin ? target.win : target.unix;
};

module.exports = getCommandForSystem;