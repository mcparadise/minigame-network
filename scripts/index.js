const DEPENDENCY_MAPPINGS = require("./dependency-mappings");
const execWrapper = require("./wrap-exec");
const getCommandForSystem = require("./platform-command-bindings");
const fs = require("fs");
const fsUtil = require("./fs-util");
const {promisify} = require("util");
const ncp = require("ncp");
const PLUGIN_MAPPINGS = require("./plugin-mappings");
const {spawn} = require("child_process");

const _COMMAND = process.argv[2];
const _TARGET = process.argv[3];

let prebuildRan = false;

/**
 * A list of things that are already built
 */
const BUILT = [];

const preBuild = async () => {
  if(prebuildRan)
    return;

  prebuildRan = true;

  console.log("Running prebuild...");

  if(await promisify(fs.exists)("../bin")) {
    fsUtil.deleteFolderRecursive("../bin");

    console.log("Removed old build folder.");
  }

  await promisify(fs.mkdir)("../bin");

  console.log("Prebuild completed.");
};

const build = async target => {
  await _build(target);

  console.log(`Main build finished, now cleaning up built modules: ${BUILT.join(",")}.`)

  // Clean up
  for(const built of BUILT) {
    console.log(`Cleaning up ${built}.`);

    try {
      fsUtil.deleteFolderRecursive(`../${built}/build`);
    } catch (e) {
      console.log(`Failed to remove ${built} build folder.`);
    }
  }
};

const _build = async target => {
  console.log(`Attempting build for target ${target}.`)

  await preBuild();

  if(BUILT.includes(target)) {
    console.log(`Skipping ${target} as it is already built.`);

    return;
  }

  BUILT.push(target);

  // Find the dependency mappings
  const buildList = DEPENDENCY_MAPPINGS[target];

  if(buildList) {
    for(const subTarget of buildList) {
      await _build(subTarget);
    }
  }

  // Build this
  const commandStages = [
    `cd ../${target}`,
    `ant all`,
  ];
  await execWrapper(commandStages.join("&&"));

  await promisify(ncp)(`../${target}/build/jar`, "../bin");
};

const createContainer = async target => {
  // Build the target
  await build(target);

  console.log("Checking for current container...");
  const dockerImages = (await execWrapper("docker image list", true)).split("\n");
  dockerImages.shift();
  for(const image of dockerImages) {
    if(!image.startsWith(`mcp/${target}`)) {
      continue;
    }

    const parts = image.split(" ").filter(part => part.length !== 0);
    const hash = parts[2];

    console.log(`Removing current docker image ${hash}`);

    // Remove this image
    await execWrapper(`docker image remove --force ${hash}`);
  }

  console.log("Creating server container...");

  // Create the server cache folder
  await promisify(fs.mkdir)(`../bin/${target}-servertemplate`);

  // Copy the server template
  await promisify(ncp)(`../servertemplate`, `../bin/${target}-servertemplate`);

  // STANDARD LIB COPIES
  await promisify(fs.copyFile)(`../lib/spigot-1.8.8.jar`, `../bin/${target}-servertemplate/spigot.jar`);

  const TO_COPY = [
    "ViaVersion-2.2.3.jar",
    "ProtocolLib.jar",
  ];

  for(const copyItem of TO_COPY) {
    await promisify(fs.copyFile)(`../lib/${copyItem}`, `../bin/${target}-servertemplate/plugins/${copyItem}`);
  }
  // END STANDARD LIB COPIES

  // Copy in minigames stuff over this
  await promisify(ncp)(`../${target}/servertemplate`, `../bin/${target}-servertemplate`);

  // Copy all of the plugin mappings
  for(const pluginTarget of [PLUGIN_MAPPINGS[target], target].flat(Infinity)) {
    await promisify(fs.copyFile)(`../bin/${pluginTarget}.jar`, `../bin/${target}-servertemplate/plugins/${pluginTarget}.jar`);
  }

  // Create the container
  await execWrapper([
    `cd ../bin/${target}-servertemplate`,
    `docker build --tag mcp/${target} .`,
  ].join("&&"));
};

const runContainer = async target => {
  // Build / Create the container
  await COMMAND_MAPPINGS.createContainer(target); // This includes the clean up

  // Run the container
  if(process.platform === "win32") {
    // Windows
    await execWrapper(`start cmd /k "docker run -it -p 25565:25565 mcp/${target}&&exit"`);

    // List containers
    const containers = (await execWrapper(`docker container list`, true)).split("\n");
    containers.shift();
    for(const container of containers) {
      const cleanedContainerName = container.split(" ").filter(item => item.length !== 0);

      if(cleanedContainerName[1] !== `mcp/${target}`)
        continue;

      await execWrapper(`docker kill ${cleanedContainerName[0]}`);
    }
  } else {
    console.log(`Other OS are not supported with inline run. Please run the command in terminal: docker run mcp/${target}`);
  }
};

const cleanUp = () => {
  fsUtil.deleteFolderRecursive("../bin");
};

const COMMAND_MAPPINGS = {
  build, // Don't clean up this
  createContainer: async target => {
    await createContainer(target);
    await cleanUp();
  },
  runContainer,
};

const _run = async () => {
  console.log("Running", _COMMAND, _TARGET);

  const startTime = Date.now();
  await COMMAND_MAPPINGS[_COMMAND](_TARGET);

  console.log(`Done in ${Math.floor((Date.now() - startTime) / 1000)}s.`);
};
_run();