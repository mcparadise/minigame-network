const DEPENDENCY_MAPPINGS = {
  shared: [],
  core: ["shared"],
  "waterwars-shared": ["core"],
  waterwars: ["core", "waterwars-shared"],
  hub: ["core", "waterwars-shared"],
  bungee: ["shared"],
  splegg: ["core"],
  deathswaps: ["core"],
};

module.exports = DEPENDENCY_MAPPINGS;