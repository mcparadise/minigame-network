const {exec} = require("child_process");

const execWrapper = async (command, returnOutput = false) => {
  const output = await _execWrapper(command);

  if(!returnOutput)
    console.log("Command output:", output);
  else
    return output;
};

const _execWrapper = command => {
  console.log(`Running console command ${command}.`)

  return new Promise((res, rej) => {
    exec(command, (err, stdout, stderr) => {
      if(err) {
        console.warn(err);
      }

      res(stdout ? stdout : stderr);
    });
  });
};

module.exports = execWrapper;