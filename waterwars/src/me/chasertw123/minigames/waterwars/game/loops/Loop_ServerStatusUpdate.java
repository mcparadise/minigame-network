package me.chasertw123.minigames.waterwars.game.loops;

import me.chasertw123.minigames.core.api.CoreAPI;
import me.chasertw123.minigames.waterwars.game.GameManager;

public class Loop_ServerStatusUpdate extends GameLoop {

    public Loop_ServerStatusUpdate() {
        super(1, 20 * 15);
    }

    @Override
    public void run() {
        CoreAPI.getServerDataManager().updateServerState(null, GameManager.MAX_PLAYERS);
    }
}
