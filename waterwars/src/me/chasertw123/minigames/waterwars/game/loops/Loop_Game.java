package me.chasertw123.minigames.waterwars.game.loops;

import me.chasertw123.minigames.shared.framework.ServerGameType;
import me.chasertw123.minigames.waterwars.Main;
import me.chasertw123.minigames.waterwars.game.GameManager;
import me.chasertw123.minigames.waterwars.game.GameState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;

/**
 * Created by Scott Hiett on 4/22/2017.
 */
public class Loop_Game extends GameLoop {

    public static final int GAME_LENGTH = 60 * 10;

    public Loop_Game(){
        super(GAME_LENGTH, 20L);
    }

    @Override
    public void run() {

        if (Main.getGameManager().inCages())
            return;

        if (interval < GAME_LENGTH && interval % 45 == 0)
            Main.getGameManager().getAliveUsers().forEach(wwUser ->
                    wwUser.getCoreUser().giveCoinsAndExperience(ServerGameType.WATER_WARS, 4, "Playtime"));

        if (Main.getGameManager().getGameState() == GameState.GAME)
            Main.getGameManager().checkGame();

        if(interval == GAME_LENGTH / 2) {
            // Restock tier 2 chests
            Main.getGameManager().getChestFiller().fillTier(2);
            Bukkit.broadcastMessage(ChatColor.AQUA + "Tier 2 chests have been restocked!");

            // Strike a lightning effect
            for(Location l : Main.getGameManager().getMap().getTier2chests()) {
                Main.getMapManager().getGameWorld().strikeLightningEffect(l);
            }
        }

        if(interval == GAME_LENGTH / 4) {
            // Restock tier 3 and tier 2
            Main.getGameManager().getChestFiller().fillTier(2);
            Main.getGameManager().getChestFiller().fillTier(3);
            Bukkit.broadcastMessage(ChatColor.AQUA + "Tier 2 & 3 chests have been restocked!");

            // Strike a lightning effect
            for(Location l : Main.getGameManager().getMap().getTier2chests()) {
                Main.getMapManager().getGameWorld().strikeLightningEffect(l);
            }
            for(Location l : Main.getGameManager().getMap().getTier3chests()) {
                Main.getMapManager().getGameWorld().strikeLightningEffect(l);
            }
        }

        if (--interval == 0) {
            Main.getGameManager().startDeathmatch();

            this.cancel();
        }
    }

    public int getInterval() {
        return interval;
    }
}
