package me.chasertw123.minigames.waterwars.game.loops;

import me.chasertw123.minigames.core.api.minigames.MinigameUtils;
import me.chasertw123.minigames.core.api.minigames.spectating.SpectateAPI;
import me.chasertw123.minigames.core.api.misc.Title;
import me.chasertw123.minigames.core.api.misc.cItemStack;
import me.chasertw123.minigames.waterwars.Main;
import me.chasertw123.minigames.waterwars.game.GameState;
import me.chasertw123.minigames.waterwars.users.WaterWarsUser;
import me.chasertw123.minigames.wws.unlocks.kits.Kit;
import net.minecraft.server.v1_8_R3.MathHelper;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.bukkit.Bukkit.getServer;

/**
 * Created by Chase on 8/3/2017.
 */
public class Loop_Respawn extends GameLoop {

    private WaterWarsUser user;

    private final List<PotionEffect> potionEffects = new ArrayList<>(Arrays.asList(
            new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 200, 3),
            new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 200, 3),
            new PotionEffect(PotionEffectType.ABSORPTION, 200, 4)));

    public Loop_Respawn(WaterWarsUser user) {
        super(12, 20);
        this.user = user;
    }

    @Override
    public void run() {

        Title.sendActionbar(user.getPlayer(), ChatColor.AQUA + "" + ChatColor.BOLD + "(!) " + ChatColor.GREEN + "You will respawn in " + interval + "s...");

        if (Main.getGameManager().getGameState() == GameState.ENDING) {
            this.cancel();
            return;
        }

        if (--interval == 0) {
            Location spawnLoc = Main.getGameManager().getMap().getIslandData(user.getIsland()).getSpawn();

            Main.getGameManager().getChestFiller().fillIsland(Main.getGameManager().getMap().getIslandData(user.getIsland()));
            Main.getGameManager().getMap().getIslandData(user.getIsland()).respawnOres();
            Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> this.knockbackPlayers(spawnLoc, user), 15L);

            user.getPlayer().closeInventory();
            Title.sendActionbar(user.getPlayer(), null);
            MinigameUtils.resetPlayer(user.getPlayer());

            Bukkit.getOnlinePlayers().stream().filter(player -> player.getUniqueId() != user.getPlayer().getUniqueId()).forEach(player -> player.showPlayer(user.getPlayer()));
            user.getPlayer().getInventory().addItem(new cItemStack(Material.COMPASS).setDisplayName(ChatColor.GREEN + "Player Tracker " + ChatColor.GRAY + "(Hold for Info)"));
            Arrays.stream(user.getSelectedKit().getItems()).forEach(item -> user.getPlayer().getInventory().addItem(item));

            user.getPlayer().teleport(spawnLoc);
            user.getPlayer().sendMessage(ChatColor.RED + "You have " + ChatColor.WHITE + user.getLives() + ChatColor.RED + (user.getLives() == 1 ? " life" : " lives") + " left!");
            user.getPlayer().spigot().setCollidesWithEntities(true);
            user.getPlayer().setGameMode(GameMode.SURVIVAL);
            user.getPlayer().addPotionEffects(potionEffects);
            user.setDead(false);

            SpectateAPI.refreshTeleporterGUI();

            this.cancel();
        }
    }

    private void knockbackPlayers(Location spawnLocation, WaterWarsUser ignoredPlayer) {

        Block currentBlock = spawnLocation.getBlock();
        while (currentBlock.getType() == null || currentBlock.getType() == Material.AIR)
            currentBlock = currentBlock.getRelative(BlockFace.DOWN);

        Location knockbackCenter = currentBlock.getLocation().clone().subtract(0f, 1f , 0f);

        boolean playedEffect = false;
        for (WaterWarsUser wwu : Main.getGameManager().getAliveUsers()) {
            if (wwu.isDead() || wwu.getPlayer().getUniqueId() == ignoredPlayer.getPlayer().getUniqueId())
                continue;

            double distance = wwu.getPlayer().getLocation().distanceSquared(knockbackCenter);
            if (distance > 144)
                continue;

            playedEffect = true;

            double t = Math.sqrt(distance) / 12d;
            double str = (3.5d * (1.0d - t)) + (1.5d * t);

            wwu.getPlayer().setVelocity(wwu.getPlayer().getLocation().toVector().subtract(knockbackCenter.toVector())
                    .normalize().setY(0).multiply(str).setY(1f));
        }

        if (playedEffect)
            knockbackCenter.getWorld().playSound(knockbackCenter, Sound.FIREWORK_LARGE_BLAST, 1.75f, 1.2f);
    }
}
