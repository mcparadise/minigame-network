package me.chasertw123.minigames.waterwars.game.loops;

import me.chasertw123.minigames.core.api.minigames.MinigameUtils;
import me.chasertw123.minigames.core.api.minigames.spectating.SpectateAPI;
import me.chasertw123.minigames.core.user.data.stats.Stat;
import me.chasertw123.minigames.shared.framework.ServerGameType;
import me.chasertw123.minigames.waterwars.users.WaterWarsUser;
import org.bukkit.*;

import java.util.Random;

/**
 * Created by Chase on 8/4/2017.
 */
public class Loop_GameOver extends GameLoop {

    private Random rand = new Random();
    private int radius = 32;

    public Loop_GameOver(WaterWarsUser winner) {
        super(1, 20);

        SpectateAPI.setCanUseTeleporter(false);
        Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.ENDERDRAGON_GROWL, 0.75F, 1F));

        winner.getCoreUser().addActivity("1st Water Wars");
        winner.getCoreUser().incrementStat(Stat.WATER_WARS_PLAYTIME, (int) (System.currentTimeMillis() - winner.getJoinTime()) / 1000);
        winner.getCoreUser().incrementStat(Stat.WATER_WARS_SOLO_GAMES_WON);
        winner.getCoreUser().giveCoinsAndExperience(ServerGameType.WATER_WARS, 50);
        winner.setDead(true);

        MinigameUtils.Messages.sendGameOverMessage(winner.getCoreUser());
        SpectateAPI.setSpectating(winner.getCoreUser());
        MinigameUtils.showAllPlayers();
    }

    @Override
    public void run() {
        MinigameUtils.playFireworks();
    }

}
