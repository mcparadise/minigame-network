package me.chasertw123.minigames.waterwars.game.maps;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Scott Hiett on 7/31/2017.
 */
public class IslandData {

    private Location spawn;
    private List<Location> chests;
    private Map<Location, Material> ores;

    public IslandData (List<Location> spawns, List<Location> chests) {
        if(spawns.size() > 1)
            spawn = spawns.get(new Random().nextInt(spawns.size()));
        else
            spawn = spawns.get(0);

        this.chests = chests;
        this.ores = new HashMap<>();

        Block block = spawn.getBlock();
        while (block.getType() == null || block.getType() == Material.AIR)
            block = block.getRelative(BlockFace.DOWN);

        for (int x = -10; x <= 10; x++)
            for (int z = -10; z <= 10; z++)
                for (int y = -15; y <= 1; y++) {
                    Location loc = block.getLocation().clone().add(x, y, z);
                    if (loc.getBlock().getType().toString().contains("ORE"))
                        ores.put(loc, loc.getBlock().getType());
                }
    }

    public List<Location> getChests() {
        return chests;
    }

    public Location getSpawn() {
        return spawn;
    }

    public void respawnOres() {
        ores.keySet().forEach(location -> location.getBlock().setType(ores.get(location)));
    }

}
