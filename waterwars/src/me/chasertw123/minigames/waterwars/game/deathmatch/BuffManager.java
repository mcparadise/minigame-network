package me.chasertw123.minigames.waterwars.game.deathmatch;

import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BuffManager {

    private static final BuffPotionEffect[] buffPotionEffects = new BuffPotionEffect[]{
            new BuffPotionEffect(PotionEffectType.SPEED, 2),
            new BuffPotionEffect(PotionEffectType.REGENERATION, 1),
            new BuffPotionEffect(PotionEffectType.ABSORPTION, 4),
    };

    private final List<BuffPotionEffect> randomizedArray;

    public BuffManager() {
        randomizedArray = Arrays.asList(buffPotionEffects);
        Collections.shuffle(randomizedArray);
    }

    public List<BuffPotionEffect> getRandomizedArray() {
        return randomizedArray;
    }

}
