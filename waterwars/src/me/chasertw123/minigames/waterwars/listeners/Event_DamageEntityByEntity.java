package me.chasertw123.minigames.waterwars.listeners;

import me.chasertw123.minigames.waterwars.Main;
import me.chasertw123.minigames.waterwars.users.WaterWarsUser;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * Created by Scott Hiett on 4/22/2017.
 */
public class Event_DamageEntityByEntity implements Listener {

    public Event_DamageEntityByEntity(){
        Bukkit.getServer().getPluginManager().registerEvents(this, Main.getInstance());
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e){

        if (e.getEntity() instanceof Wolf) {
            if (e.getDamager() instanceof Player) {
                WaterWarsUser user = Main.getUserManager().get(e.getDamager().getUniqueId());

                if (user.isDead() || ((Wolf) e.getEntity()).getOwner().getUniqueId() == e.getDamager().getUniqueId()) {
                    e.setCancelled(true);
                    return;
                }
            }

            if (e.getDamage() >= ((Wolf) e.getEntity()).getHealth()) {
                e.setCancelled(true);
                e.getEntity().getLocation().getWorld().playSound(e.getEntity().getLocation(), Sound.WOLF_DEATH, 1.f, 1.f);
                e.getEntity().getLocation().getWorld().playEffect(e.getEntity().getLocation(), Effect.SMALL_SMOKE, 3);
                e.getEntity().remove();

                return;
            }
        }

        else if (e.getEntity() instanceof Player) {
            if (e.getDamager() instanceof Wolf) {
                e.setDamage(0);

                if (!(((Wolf) e.getDamager()).getOwner() instanceof Player))
                    return;

                ((Player) e.getEntity()).damage(e.getDamage(), (Player) ((Wolf) e.getDamager()).getOwner());
            }

            else if (e.getDamager() instanceof Player && Main.getUserManager().get(e.getDamager().getUniqueId()).isDead())
                e.setCancelled(true);
        }


    }

}
