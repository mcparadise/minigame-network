package me.chasertw123.minigames.waterwars.listeners;

import me.chasertw123.minigames.core.api.minigames.spectating.SpectateAPI;
import me.chasertw123.minigames.waterwars.Main;
import me.chasertw123.minigames.waterwars.game.GameState;
import me.chasertw123.minigames.waterwars.users.WaterWarsUser;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by Chase on 7/30/2017.
 */
public class Event_EntityDamage implements Listener {

    public Event_EntityDamage(){
        Bukkit.getServer().getPluginManager().registerEvents(this, Main.getInstance());
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.isCancelled() || Main.getGameManager().getGameState() != GameState.GAME)
            return;

        if (event.getEntity() instanceof Player) {
            WaterWarsUser wwu = Main.getUserManager().get(event.getEntity().getUniqueId());
            if (wwu.isDead() || event.getCause() == EntityDamageEvent.DamageCause.FALL)
                event.setCancelled(true);

            if (!wwu.isFullDead())
                SpectateAPI.refreshTeleporterGUI();
        }

        else if (event.getEntity() instanceof Wolf) {
            if (event.getDamage() < ((Wolf) event.getEntity()).getHealth())
                return;

            event.getEntity().getLocation().getWorld().playSound(event.getEntity().getLocation(), Sound.WOLF_DEATH, 1f, 1f);
            event.setCancelled(true);
            event.getEntity().remove();
        }
    }
}
