package me.chasertw123.minigames.waterwars.listeners;

import me.chasertw123.minigames.core.api.minigames.MinigameUtils;
import me.chasertw123.minigames.core.api.CoreAPI;
import me.chasertw123.minigames.core.api.minigames.spectating.SpectateAPI;
import me.chasertw123.minigames.core.api.misc.AbstractItem;
import me.chasertw123.minigames.core.api.misc.Item;
import me.chasertw123.minigames.core.api.misc.cItemStack;
import me.chasertw123.minigames.waterwars.Main;
import me.chasertw123.minigames.waterwars.game.GameManager;
import me.chasertw123.minigames.waterwars.game.GameState;
import me.chasertw123.minigames.waterwars.game.scoreboards.SB_Game;
import me.chasertw123.minigames.waterwars.game.scoreboards.SB_Lobby;
import me.chasertw123.minigames.waterwars.guis.Gui_CageSelector;
import me.chasertw123.minigames.waterwars.guis.Gui_KitSelector;
import me.chasertw123.minigames.waterwars.guis.Gui_MapVote;
import me.chasertw123.minigames.waterwars.users.WaterWarsUser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Chase on 2/19/2017.
 */
public class Event_PlayerJoin implements Listener {

    private Gui_MapVote mapVoteGUI = new Gui_MapVote();
    private AbstractItem mapVoting = new AbstractItem(Item.MAP_VOTING, (user, clickType) -> mapVoteGUI.openInventory(user));

    private AbstractItem kitSelect = new AbstractItem(new cItemStack(Material.ARMOR_STAND, ChatColor.GREEN + "Kit Selector " + ChatColor.GRAY + "(Right Click)"),
            (user, clickType) -> new Gui_KitSelector(Main.getUserManager().get(user.getUUID())));

    private AbstractItem cageSelect = new AbstractItem(new cItemStack(Material.GLASS, ChatColor.AQUA + "Cage Selector " + ChatColor.GRAY + "(Right Click)"),
            (user, clickType) -> new Gui_CageSelector(Main.getUserManager().get(user.getUUID())));

    public Event_PlayerJoin() {
        Main.getInstance().getServer().getPluginManager().registerEvents(this, Main.getInstance());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        if (Main.getGameManager().getGameState() == GameState.LOBBY && Bukkit.getServer().getOnlinePlayers().size() == GameManager.MAX_PLAYERS + 1) {
            e.getPlayer().kickPlayer("This server is at max capacity and not yet in game to spectate!");
            return;
        }

        WaterWarsUser user = new WaterWarsUser(CoreAPI.getUser(e.getPlayer()));
        Main.getUserManager().add(user);

        if (Main.getGameManager().getGameState() != GameState.LOBBY) {

            e.setJoinMessage(null);
            e.getPlayer().teleport(Main.getGameManager().getMap().getMapCenter());

            user.setLives(0);
            user.setDead(true);
            user.getCoreUser().setScoreboard(new SB_Game(user));

            SpectateAPI.setSpectating(user.getCoreUser());

            user.getPlayer().setPlayerListName(ChatColor.DARK_GRAY + "[" +  (user.getLives() == 0 ? ChatColor.RED + "DEAD" : ChatColor.YELLOW + ""
                    + user.getLives()) + ChatColor.DARK_GRAY + "] " + user.getCoreUser().getRank().getRankColor() + user.getPlayer().getName());

            return;
        }

        user.getCoreUser().setScoreboard(new SB_Lobby(user));

        net.md_5.bungee.api.ChatColor nameColor = user.getCoreUser().getRank().getRankColor();
        if (!user.getCoreUser().getRank().isStaff() && user.getCoreUser().isDeluxe())
            nameColor = net.md_5.bungee.api.ChatColor.GOLD;

        e.setJoinMessage(nameColor + e.getPlayer().getName() + ChatColor.GRAY + " joined the game! (" + Main.getUserManager().toCollection().size() + "/" + GameManager.MAX_PLAYERS + ")");

        MinigameUtils.resetPlayer(user.getPlayer());
        MinigameUtils.showAllPlayers();
        user.getPlayer().spigot().setCollidesWithEntities(true);
        e.getPlayer().teleport(Main.getMapManager().getLobbyWorld().getSpawnLocation());
        e.getPlayer().setPlayerListName(user.getCoreUser().getRank().getRankColor() + e.getPlayer().getName());

        Item.HOW_TO_PLAY.give(user.getPlayer(), 0);
        mapVoting.give(user.getPlayer(), 1);
        kitSelect.give(user.getPlayer(), 2);
        cageSelect.give(user.getPlayer(), 3);
        Item.RETURN_TO_HUB.give(user.getPlayer(), 8);

        CoreAPI.getServerDataManager().updateServerState(null, GameManager.MAX_PLAYERS);
    }
}
