package me.chasertw123.minigames.waterwars.listeners;

import me.chasertw123.minigames.waterwars.Main;
import me.chasertw123.minigames.waterwars.game.GameState;
import me.chasertw123.minigames.waterwars.users.WaterWarsUser;
import me.chasertw123.minigames.wws.unlocks.kits.Kit;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.SmallFireball;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Random;

/**
 * Created by Scott Hiett on 4/21/2017.
 */
public class Event_PlayerInteract implements Listener {

    private Random rand;

    public Event_PlayerInteract() {
        rand = new Random();
        Bukkit.getServer().getPluginManager().registerEvents(this, Main.getInstance());
    }

    @EventHandler (priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent e) {

        if (Main.getGameManager().getGameState() == GameState.GAME
                && e.getAction() == Action.RIGHT_CLICK_BLOCK
                && !Main.getGameManager().getMap().canInteractWithLocation(e.getClickedBlock().getLocation())) {
            e.setCancelled(true);
            Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> e.getPlayer().updateInventory(), 1L);
        }


        WaterWarsUser user = Main.getUserManager().get(e.getPlayer().getUniqueId());
        if ((user.isDead() || user.isFullDead()) && e.getAction() == Action.RIGHT_CLICK_BLOCK && (e.getClickedBlock().getType() == Material.CHEST || e.getClickedBlock().getType() == Material.FURNACE
                || e.getClickedBlock().getType() == Material.BREWING_STAND || e.getClickedBlock().getType() == Material.ENCHANTMENT_TABLE || e.getClickedBlock().getType() == Material.ENDER_CHEST
                || e.getClickedBlock().getType() == Material.ANVIL || e.getClickedBlock().getType() == Material.BURNING_FURNACE || e.getClickedBlock().getType() == Material.TRAPPED_CHEST
                || e.getClickedBlock().getType() == Material.WORKBENCH || e.getClickedBlock().getType() == Material.JUKEBOX || e.getClickedBlock().getType() == Material.NOTE_BLOCK
                || e.getClickedBlock().getType() == Material.TRAP_DOOR)) {

            e.setCancelled(true);
        }

        if (e.getPlayer().getItemInHand() == null)
            return;

        if (e.getPlayer().getItemInHand().getType() == Material.FIREBALL)
        {
            if (e.getAction() != Action.RIGHT_CLICK_AIR)
                return;

            removeOneItemFromHand(e.getPlayer());
            e.setCancelled(true);

            e.getPlayer().launchProjectile(SmallFireball.class).setVelocity(e.getPlayer().getLocation().getDirection().multiply(0.5));
        }

        else if (e.getPlayer().getItemInHand().getType() == Material.MONSTER_EGG)
        {
            if (Main.getGameManager().getGameState() != GameState.GAME && user.getSelectedKit() != Kit.WOLFMAN)
                return;

            if (Main.getGameManager().inCages())
                return;

            if (e.getAction() != Action.RIGHT_CLICK_BLOCK)
                return;

            removeOneItemFromHand(e.getPlayer());
            e.setCancelled(true);

            Wolf wolf = (Wolf) e.getPlayer().getLocation().getWorld().spawnEntity(e.getPlayer().getLocation(), EntityType.WOLF);
            wolf.setMaxHealth(wolf.getMaxHealth() * 3);
            wolf.setHealth(wolf.getMaxHealth());
            wolf.setCollarColor(DyeColor.values()[rand.nextInt(DyeColor.values().length - 1)]);
            wolf.setSitting(false);
            wolf.setAdult();
            wolf.setCustomName(user.getCoreUser().getUsername() + "'s Wolf");
            wolf.setCustomNameVisible(true);
            wolf.setOwner(e.getPlayer());
        }

    }

    private void removeOneItemFromHand(Player player)
    {
        if (player.getItemInHand().getAmount() > 1) {
            player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
        } else {
            //player.getInventory().remove(new ItemStack(player.getItemInHand().getType(), 1));
            player.setItemInHand(null);
        }

        player.updateInventory();
    }

}
