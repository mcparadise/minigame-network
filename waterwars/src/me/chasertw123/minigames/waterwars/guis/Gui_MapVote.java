package me.chasertw123.minigames.waterwars.guis;

import me.chasertw123.minigames.core.api.misc.AbstractGUI;
import me.chasertw123.minigames.core.api.misc.cItemStack;
import me.chasertw123.minigames.waterwars.Main;
import me.chasertw123.minigames.waterwars.game.maps.BaseMap;
import me.chasertw123.minigames.waterwars.users.WaterWarsUser;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;

/**
 * Created by Chase on 4/23/2017.
 */
public class Gui_MapVote extends AbstractGUI {

    public Gui_MapVote() {
        super(1, "Vote for a map!");
        refreshGUI();
    }

    private void refreshGUI() {
        int i = 1;
        for (BaseMap map : Main.getMapManager().getMaps()) {
            setItem(new cItemStack(Material.EMPTY_MAP, ChatColor.YELLOW + map.getName() + ChatColor.WHITE + " [" + ChatColor.GREEN
                    + Main.getVoteManager().getVotes(map) + ChatColor.WHITE + "]"), i, (user, clickType) -> {

                user.getPlayer().closeInventory();

                if (!Main.getVoteManager().isVotingActive()) {
                    user.getPlayer().sendMessage(ChatColor.RED + "Voting has already ended!");
                    user.getPlayer().playSound(user.getPlayer().getLocation(), Sound.VILLAGER_NO, 1f, 1f);
                    return;
                }

                WaterWarsUser wwUser = Main.getUserManager().get(user.getUUID());
                if (wwUser.getVotedMap() != null && wwUser.getVotedMap().equalsIgnoreCase(map.getName())) {
                    user.getPlayer().sendMessage(ChatColor.WHITE + "You already voted for the map " + ChatColor.AQUA + map.getName().toUpperCase() + ChatColor.WHITE + "!");
                    user.getPlayer().playSound(user.getPlayer().getLocation(), Sound.VILLAGER_HAGGLE, 1f, 1f);
                    return;
                }

                user.getPlayer().sendMessage(ChatColor.WHITE + "You voted for the map " + ChatColor.AQUA + map.getName().toUpperCase() + ChatColor.WHITE + "!");
                user.getPlayer().playSound(user.getPlayer().getLocation(), Sound.VILLAGER_YES, 1f, 1f);

                wwUser.setVotedMap(map.getName());
                refreshGUI();
            });

            i += 3;
        }
    }
}
