package me.chasertw123.minigames.waterwars.guis;

import me.chasertw123.minigames.core.api.misc.AbstractGUI;
import me.chasertw123.minigames.waterwars.unlocks.perks.PerkType;
import me.chasertw123.minigames.waterwars.users.WaterWarsUser;

/**
 * Created by Chase on 5/2/2017.
 */
public class Gui_Perks extends AbstractGUI {

    public Gui_Perks(WaterWarsUser user) {
        super((PerkType.values().length / 9) + 1, "Your Perks", user.getCoreUser());

        int i = 0;
        for (PerkType perkType : PerkType.values())
            setItem(perkType.getPerkClass().getDisplay(user), i++, (user1, clickType) -> {}); //TODO: Set actions when you click
    }
}
