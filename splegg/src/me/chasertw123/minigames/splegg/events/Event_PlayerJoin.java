package me.chasertw123.minigames.splegg.events;

import me.chasertw123.minigames.core.api.minigames.MinigameUtils;
import me.chasertw123.minigames.core.api.minigames.spectating.SpectateAPI;
import me.chasertw123.minigames.core.api.misc.Item;
import me.chasertw123.minigames.core.api.misc.cItemStack;
import me.chasertw123.minigames.shared.framework.ServerSetting;
import me.chasertw123.minigames.splegg.Main;
import me.chasertw123.minigames.splegg.game.GameManager;
import me.chasertw123.minigames.splegg.game.boards.SB_Game;
import me.chasertw123.minigames.splegg.game.boards.SB_Lobby;
import me.chasertw123.minigames.splegg.game.states.GameState;
import me.chasertw123.minigames.splegg.users.SpleggPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Scott Hiett on 8/7/2017.
 */
public class Event_PlayerJoin implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void playerJoin(PlayerJoinEvent e) {

        if (Main.getInstance().gameManager.getGameState() == GameState.LOBBY && Bukkit.getServer().getOnlinePlayers().size() == GameManager.MAX_PLAYERS + 1) {
            e.getPlayer().kickPlayer("This server is at max capacity and not yet in game to spectate!");
            return;
        }

        SpleggPlayer player = new SpleggPlayer(e.getPlayer().getUniqueId());
        Main.getInstance().spleggPlayerManager.add(player);

        if (Main.getInstance().gameManager.getGameState() != GameState.LOBBY)
        {
            e.setJoinMessage(null);
            e.getPlayer().teleport(Main.getInstance().gameManager.getGameMap().getSpawns().get(0));
            player.setDead(true);
            player.getCoreUser().setScoreboard(new SB_Game(player));
            SpectateAPI.setSpectating(player.getCoreUser());

            return;
        }

        player.getCoreUser().setServerSetting(ServerSetting.DAMAGE, false);
        player.getCoreUser().setServerSetting(ServerSetting.HUNGER, false);
        player.getCoreUser().setServerSetting(ServerSetting.ITEM_DROPPING, false);

        MinigameUtils.resetPlayer(e.getPlayer());
        MinigameUtils.showAllPlayers();

        e.setJoinMessage(Main.PREFIX + ChatColor.GOLD + "" + ChatColor.BOLD + e.getPlayer().getName() + ChatColor.RESET
                + " joined the game. " + ChatColor.GRAY + "(" + Bukkit.getServer().getOnlinePlayers().size() + "/"
                + GameManager.MAX_PLAYERS + ")");

        player.getCoreUser().setScoreboard(new SB_Lobby(player));

        e.getPlayer().teleport(Main.getInstance().mapManager.getLobbyWorld().getSpawnLocation().clone().add(0.5, 0, 0.5));

        Item.HOW_TO_PLAY.give(e.getPlayer(), 0);

        e.getPlayer().getInventory().setItem(1, new cItemStack(Material.EMPTY_MAP, ChatColor.YELLOW + "Vote for Map " + ChatColor.GRAY + "(Right Click)"));
        e.getPlayer().getInventory().setItem(2, new cItemStack(Material.IRON_SPADE, ChatColor.GOLD + "Select Shovel " + ChatColor.GRAY + "(Right Click)"));
        e.getPlayer().getInventory().setItem(3, new cItemStack(Material.BLAZE_POWDER, ChatColor.GREEN + "Select Powerup " + ChatColor.GRAY + "(Right Click)"));
        e.getPlayer().getInventory().setItem(4, new cItemStack(Material.NOTE_BLOCK, ChatColor.AQUA + "Select Trail " + ChatColor.GRAY + "(Right Click)"));

        Item.RETURN_TO_HUB.give(e.getPlayer(), 8);
    }

}
