package me.chasertw123.minigames.splegg.game.boards;

import me.chasertw123.minigames.core.api.scoreboard.Entry;
import me.chasertw123.minigames.core.api.scoreboard.EntryBuilder;
import me.chasertw123.minigames.core.api.scoreboard.ScoreboardHandler;
import me.chasertw123.minigames.core.api.scoreboard.SimpleScoreboard;
import me.chasertw123.minigames.splegg.Main;
import me.chasertw123.minigames.splegg.users.SpleggPlayer;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public class SB_Game extends SimpleScoreboard {

    public SB_Game(SpleggPlayer spleggPlayer) {
        super(spleggPlayer.getPlayer());

        this.setUpdateInterval(5L);
        this.setHandler(new ScoreboardHandler() {
            @Override
            public String getTitle(Player player) {
                int time = Main.getInstance().gameManager.gameLoop.interval, minutes = time / 60, seconds = time % 60;
                String mins = (minutes < 10 ? "0" : "") + minutes, secs = (seconds < 10 ? "0" : "") + seconds;

                return ChatColor.GOLD + "" + ChatColor.BOLD + "Splegg " + ChatColor.GREEN + "" + mins + ":" + secs;
            }

            @Override
            public List<Entry> getEntries(Player player) {

                EntryBuilder eb = new EntryBuilder();

                eb.blank();
                eb.next(ChatColor.YELLOW + "" + ChatColor.BOLD + "Game Info");
                eb.next(ChatColor.WHITE + "Players Left: " + ChatColor.GREEN + Main.getInstance().gameManager.getAliveSpleggPlayers().size());
                eb.blank();
                eb.next(ChatColor.GREEN + "" + ChatColor.BOLD + "My Info");
                eb.next(ChatColor.WHITE + "Blocks Broken: " + ChatColor.GREEN + spleggPlayer.getBlocksBroken());
                eb.next(ChatColor.WHITE + "Eggs Shot: " + ChatColor.GREEN + spleggPlayer.getEggsShot());

                return eb.build();
            }
        });
    }

}
