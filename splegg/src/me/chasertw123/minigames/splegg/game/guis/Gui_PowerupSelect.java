package me.chasertw123.minigames.splegg.game.guis;

import me.chasertw123.minigames.core.api.misc.AbstractGUI;
import me.chasertw123.minigames.shared.utils.StringUtil;
import me.chasertw123.minigames.splegg.Main;
import me.chasertw123.minigames.splegg.game.powerups.PowerupType;
import me.chasertw123.minigames.splegg.users.SpleggPlayer;
import org.bukkit.ChatColor;

/**
 * Created by Scott Hiett on 8/8/2017.
 */
public class Gui_PowerupSelect extends AbstractGUI {

    public Gui_PowerupSelect(SpleggPlayer spleggPlayer) {
        super(1, "Select Powerup", spleggPlayer.getCoreUser());

        int count = 0;
        for (PowerupType powerupType : PowerupType.values()) {
            setItem(powerupType.getPowerupClass().getItemRepresentation(), count, (user1, clickType) -> {
                Main.getInstance().spleggPlayerManager.get(spleggPlayer.getUuid()).setPowerupType(powerupType);
                spleggPlayer.getPlayer().closeInventory();
                Main.getInstance().spleggPlayerManager.get(spleggPlayer.getUuid()).sendPrefixedMessage("Selected the Powerup "
                        + ChatColor.GOLD + "" + ChatColor.BOLD + StringUtil.niceString(powerupType.toString().replaceAll("_", " ")).toUpperCase() + ChatColor.WHITE + ".");
            });

            count++;
        }
    }

}
