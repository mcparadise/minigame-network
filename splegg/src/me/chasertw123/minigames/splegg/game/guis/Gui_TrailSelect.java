package me.chasertw123.minigames.splegg.game.guis;

import me.chasertw123.minigames.core.api.misc.AbstractGUI;
import me.chasertw123.minigames.shared.utils.StringUtil;
import me.chasertw123.minigames.splegg.game.trails.TrailType;
import me.chasertw123.minigames.splegg.users.SpleggPlayer;
import org.bukkit.ChatColor;

/**
 * Created by Scott Hiett on 8/11/2017.
 */
public class Gui_TrailSelect extends AbstractGUI {

    public Gui_TrailSelect(SpleggPlayer spleggPlayer) {
        super(1, "Trails", spleggPlayer.getCoreUser());

        int count = 0;
        for (TrailType trailType : TrailType.values()) {
            setItem(trailType.getTrail().getItemRepresentation(), count, (user1, clickType) -> {
                spleggPlayer.setTrailType(trailType);
                spleggPlayer.getPlayer().closeInventory();

                spleggPlayer.sendPrefixedMessage("Selected the Trail " + ChatColor.GOLD + "" + ChatColor.BOLD
                        + StringUtil.niceString(trailType.getTrail().getName()).toUpperCase() + ChatColor.WHITE + ".");
            });

            count++;
        }
    }

}
