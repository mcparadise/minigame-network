package me.chasertw123.minigames.splegg.game.guis;

import me.chasertw123.minigames.core.api.misc.AbstractGUI;
import me.chasertw123.minigames.core.api.misc.cItemStack;
import me.chasertw123.minigames.splegg.Main;
import me.chasertw123.minigames.splegg.users.SpleggPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;

/**
 * Created by Scott Hiett on 8/8/2017.
 */
public class Gui_PowerupOptions extends AbstractGUI {

    public Gui_PowerupOptions(SpleggPlayer user) {
        super(1, "Enable Powerups?", user.getCoreUser());

        setItem(new cItemStack(Material.EMERALD_BLOCK, ChatColor.GREEN + "" + ChatColor.BOLD + "Yes"), 2, (user1, clickType) -> {
            if (!Main.getInstance().gameManager.voteManager.isVotingActive()) {
                user.getCoreUser().getPlayer().sendMessage(Main.PREFIX + ChatColor.RED + "Voting has already ended!");
                user.getCoreUser().getPlayer().playSound(user.getPlayer().getLocation(), Sound.VILLAGER_NO, 1F, 1F);
                user.getCoreUser().getPlayer().closeInventory();
                return;
            }

            Main.getInstance().spleggPlayerManager.get(user.getPlayer().getUniqueId()).setWantsPowerups(true);
            Main.getInstance().spleggPlayerManager.get(user.getPlayer().getUniqueId()).sendPrefixedMessage("Voted for Powerups to be " + ChatColor.GOLD + "" + ChatColor.BOLD + "ENABLED" + ChatColor.RESET + ".");

            user.getPlayer().closeInventory();
        });

        setItem(new cItemStack(Material.REDSTONE_BLOCK, ChatColor.RED + "" + ChatColor.BOLD + "No"), 6, (user1, clickType) -> {
            if (!Main.getInstance().gameManager.voteManager.isVotingActive()) {
                user.getCoreUser().getPlayer().sendMessage(Main.PREFIX + ChatColor.RED + "Voting has already ended!");
                user.getCoreUser().getPlayer().playSound(user.getPlayer().getLocation(), Sound.VILLAGER_NO, 1F, 1F);
                user.getCoreUser().getPlayer().closeInventory();
                return;
            }

            Main.getInstance().spleggPlayerManager.get(user.getPlayer().getUniqueId()).setWantsPowerups(false);
            Main.getInstance().spleggPlayerManager.get(user.getPlayer().getUniqueId()).sendPrefixedMessage("Voted for Powerups to be " + ChatColor.GOLD + "" + ChatColor.BOLD + "DISABLED" + ChatColor.RESET + ".");

            user.getPlayer().closeInventory();
        });
    }

}
