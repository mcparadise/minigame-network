package me.chasertw123.minigames.splegg.game.loops;

import me.chasertw123.minigames.core.api.minigames.MinigameUtils;

/**
 * Created by Scott Hiett on 8/11/2017.
 */
public class Loop_EndGame extends GameLoop {

    public Loop_EndGame() {
        super(1, 20);
    }

    @Override
    public void run() {
        MinigameUtils.playFireworks();
    }
}
