package me.chasertw123.minigames.splegg.game.loops;

import me.chasertw123.minigames.core.api.misc.Title;
import me.chasertw123.minigames.core.user.data.achievements.Achievement;
import me.chasertw123.minigames.core.api.misc.cItemStack;
import me.chasertw123.minigames.shared.framework.ServerGameType;
import me.chasertw123.minigames.splegg.Main;
import me.chasertw123.minigames.splegg.game.modes.GameEndReason;
import me.chasertw123.minigames.splegg.game.states.GameState;
import me.chasertw123.minigames.splegg.users.SpleggPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Created by Scott Hiett on 8/7/2017.
 */
public class Loop_Game extends GameLoop {

    private Loop_PreGame loopPreGame;
    private Loop_DeathCheck loopDeathCheck;
    private int deadCount = 0;

    private int total_time;

    public Loop_Game(int time) {
        super(60 * time, 20);
        total_time = 60 * time;
        loopDeathCheck = new Loop_DeathCheck();
        loopPreGame = new Loop_PreGame();
    }

    @Override
    public void run() {
        if (Main.getInstance().gameManager.getGameState() == GameState.GAME)
            Main.getInstance().gameManager.checkGame();

        if (loopPreGame.isActive())
            return;

        if (interval < total_time && interval % 30 == 0)
            Main.getInstance().gameManager.getAliveSpleggPlayers().forEach(spleggPlayer ->
                spleggPlayer.getCoreUser().giveCoinsAndExperience(ServerGameType.SPLEGG, 4, "Playtime"));

        if (interval-- == 0)
            Main.getInstance().gameManager.endGame(GameEndReason.TIME_UP);
    }

    public void stopDeathCheck()
    {
        loopDeathCheck.cancel();
    }

    public boolean isPreGameActive()
    {
        return loopPreGame.isActive();
    }

    private class Loop_DeathCheck extends GameLoop
    {
        public Loop_DeathCheck()
        {
            super(0, 4L);
        }

        @Override
        public void run() {
            Main.getInstance().spleggPlayerManager.toCollection().forEach(spleggPlayer -> {

                if (spleggPlayer.getPlayer().getLocation().getY() > 0)
                    return;

                if (spleggPlayer.isAlive()) {
                    if (Main.getInstance().gameManager.gameLoop.isPreGameActive()) {
                        spleggPlayer.getPlayer().teleport(Main.getInstance().gameManager.getGameMap().getSpawns().get(0));
                        return;
                    }

                    Main.getInstance().gameManager.currentGameMode.getGameModeClass().onPlayerDeath(spleggPlayer);

                    if (deadCount == 0)
                        spleggPlayer.getCoreUser().unlockAchievement(Achievement.SPLEGG_FIRST_TO_DIE);

                    deadCount++;
                    Main.getInstance().gameManager.checkGame();
                }

                else {
                    spleggPlayer.getPlayer().teleport(Main.getInstance().gameManager.getGameMap().getSpawns().get(0));
                    spleggPlayer.getPlayer().playSound(spleggPlayer.getPlayer().getLocation(), Sound.ENDERMAN_TELEPORT, 1f, 1f);
                }

            });
        }
    }

    private class Loop_PreGame extends GameLoop {

        boolean active = true;

        public Loop_PreGame() {
            super(15, 20);
        }

        @Override
        public void run() {
            for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                Title.sendTitle(player, 1, 20, 1, ChatColor.YELLOW + "" + ChatColor.BOLD + "" + interval);
                player.playSound(player.getLocation(), Sound.CLICK, 2F, 2F);
            }

            if (interval-- == 0) {
                for (SpleggPlayer spleggPlayer : Main.getInstance().gameManager.getAliveSpleggPlayers()) {
                    spleggPlayer.getPlayer().getInventory().clear();
                    spleggPlayer.getPlayer().getInventory().addItem(new cItemStack(spleggPlayer.getShovelType().getMaterial(), ChatColor.WHITE + "Egg Launcher"));
                    Title.sendTitles(spleggPlayer.getPlayer(), 0, 20, 0, ChatColor.GREEN + "" + ChatColor.BOLD + "GO", ChatColor.GOLD
                            + "" + ChatColor.ITALIC + Main.getInstance().gameManager.getGameMap().getName() + " built by " + Main.getInstance().gameManager.getGameMap()
                            .getBuilders().toString().substring(1).replaceAll("]", ""));
                    spleggPlayer.getPlayer().playSound(spleggPlayer.getPlayer().getLocation(), Sound.ENDERDRAGON_GROWL, 1.25F, 1.25F);
                }

                active = false;
                this.cancel();
            }
        }

        public boolean isActive() {
            return active;
        }
    }
}
