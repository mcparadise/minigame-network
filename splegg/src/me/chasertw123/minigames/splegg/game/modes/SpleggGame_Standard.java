package me.chasertw123.minigames.splegg.game.modes;

import me.chasertw123.minigames.core.api.minigames.MinigameUtils;
import me.chasertw123.minigames.core.api.minigames.spectating.SpectateAPI;
import me.chasertw123.minigames.core.user.User;
import me.chasertw123.minigames.core.user.data.achievements.Achievement;
import me.chasertw123.minigames.core.user.data.stats.Stat;
import me.chasertw123.minigames.shared.framework.ServerGameType;
import me.chasertw123.minigames.splegg.Main;
import me.chasertw123.minigames.splegg.game.loops.Loop_EndGame;
import me.chasertw123.minigames.splegg.users.SpleggPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Created by ScottsCoding on 12/08/2017.
 */
public class SpleggGame_Standard extends AbstractSpleggGameMode {

    public SpleggGame_Standard() {
        super("Classic Splegg", "Plain old Splegg. Fire eggs at each other in an attempt to knock the " +
                "other person off, while charging up your powerups for the next attack.", new ItemStack(Material.STONE),
                8);
    }

    @Override
    public void onShoot(SpleggPlayer shooter) {}

    @Override
    public void onEggLand(Location hitBlock, SpleggPlayer shooter) {

        if (hitBlock.getBlock().getType() == Material.TNT) {
            hitBlock.getWorld().createExplosion(hitBlock, 6.0F);
            shooter.getCoreUser().unlockAchievement(Achievement.SPLEGG_TNT);
        }

        hitBlock.getBlock().setType(Material.AIR);
    }

    @Override
    public void onGameStart() {}

    @Override
    public void onGameEnd(GameEndReason reason) {

        SpectateAPI.setCanUseTeleporter(false);

        if (reason == GameEndReason.SOMEONE_WON) {
            SpleggPlayer winner = Main.getInstance().gameManager.getAliveSpleggPlayers().get(0);
            winner.getCoreUser().giveCoinsAndExperience(ServerGameType.SPLEGG, 50);
            winner.getCoreUser().addActivity("1st in Splegg");
            winner.getCoreUser().incrementStat(Stat.SPLEGG_GAMES_WON);
            winner.updatePlaytime();

            if (winner.getEggsShot() == 0)
                winner.getCoreUser().unlockAchievement(Achievement.SPLEGG_GOD);

            MinigameUtils.Messages.sendGameOverMessage(winner.getCoreUser());
            SpectateAPI.setSpectating(winner.getCoreUser());

            Bukkit.getServer().getOnlinePlayers().stream()
                    .filter(player -> player.getUniqueId() != winner.getUuid())
                    .forEach(player -> player.teleport(winner.getPlayer()));
        }

        else
            MinigameUtils.Messages.sendGameOverMessage((User) null);

        MinigameUtils.showAllPlayers();

        new Loop_EndGame();
    }

    @Override
    public void onTNTExplode(List<Block> affectedBlocks) {
        for(Block block : affectedBlocks)
            block.setType(Material.AIR);
    }

    @Override
    public void onTweetsBlockRemove(Block block) {
        block.setType(Material.AIR);
    }

    @Override
    public void onPlayerDeath(SpleggPlayer spleggPlayer) {
        Player p = spleggPlayer.getPlayer();
        spleggPlayer.setDead(true);

        SpectateAPI.refreshTeleporterGUI();

        spleggPlayer.sendPrefixedMessage(ChatColor.RED + "You have been eliminated!");
        spleggPlayer.updatePlaytime();

        int place = Main.getInstance().gameManager.getAliveSpleggPlayers().size() + 1;
        spleggPlayer.getCoreUser().addActivity(ordinal(place) + " in Splegg");

        for (SpleggPlayer s : Main.getInstance().spleggPlayerManager.toCollection())
            if(s.getPlayer().getUniqueId() != spleggPlayer.getPlayer().getUniqueId())
                s.sendPrefixedMessage(ChatColor.GOLD + "" + ChatColor.BOLD + spleggPlayer.getPlayer().getName()
                        + ChatColor.RESET + " has been " + ChatColor.YELLOW + "" + ChatColor.BOLD + "ELIMINATED" + ChatColor.RESET + "!");

        Main.getInstance().spleggPlayerManager.toCollection().stream().filter(SpleggPlayer::isAlive).forEach(player ->
                player.getCoreUser().giveCoinsAndExperience(ServerGameType.SPLEGG, 10, "Playtime"));

        SpectateAPI.setSpectating(spleggPlayer.getCoreUser());

        p.setPlayerListName(ChatColor.RED + "" + ChatColor.BOLD + "DEAD " + ChatColor.RESET + p.getName());
        p.getLocation().getWorld().strikeLightningEffect(p.getLocation());
        p.teleport(Main.getInstance().gameManager.getGameMap().getSpawns().get(0));
    }

}
