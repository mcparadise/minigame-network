package me.chasertw123.minigames.shared.user;

import me.chasertw123.minigames.shared.database.Database;
import me.chasertw123.minigames.shared.database.DatabaseDocument;
import me.chasertw123.minigames.shared.database.DatabaseField;
import me.chasertw123.minigames.shared.rank.Rank;
import org.bson.Document;

import java.util.UUID;

public class BaseUserDocument extends DatabaseDocument {

    @DatabaseField
    private String uuid;
    private UUID castUuid;

    @DatabaseField
    private int experience;

    @DatabaseField
    private Rank rank;

    @DatabaseField
    private String lastKnownUsername;

    @DatabaseField
    private long deluxe;

    public BaseUserDocument(Database database, UUID uuid) {
        super(database, Database.Collection.USERS, new Document().append("uuid", uuid.toString()));

        this.castUuid = uuid;
        this.uuid = uuid.toString();
    }

    public UUID getUUID() {
        return this.castUuid;
    }
}
