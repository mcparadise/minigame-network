package me.chasertw123.minigames.shared.database;

import java.io.PrintStream;

public class ErrorLog {

    static String SERVER_NAME = null;

    private Database database;

    public ErrorLog(Database database, String serverName) {
        this.database = database;
        SERVER_NAME = serverName;

        this.registerBinding();
    }

    public void setServerName(String serverName) {
        SERVER_NAME = serverName;
    }

    private void registerBinding() {
        PrintStream oldErr = System.err;
        System.setErr(new ErrorPrintStream(oldErr, this.database));
    }

    public Database getDatabase() {
        return database;
    }
}
