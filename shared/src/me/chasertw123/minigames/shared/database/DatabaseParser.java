package me.chasertw123.minigames.shared.database;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface DatabaseParser {

    String variableName() default "";
}
