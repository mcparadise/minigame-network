package me.chasertw123.minigames.shared.database;

import org.bson.Document;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestDatabaseDocument extends DatabaseDocument {

    public enum TestEnum {
        TEST_ENUM_VALUE
    }

    @DatabaseField
    private String someKeyName;

    @DatabaseField
    private int someValue;

    @DatabaseField
    private String anotherValue;

    @DatabaseField
    private TestEnum testEnum;

    @DatabaseField
    private List<String> exampleList;

    @DatabaseField
    private List<List<String>> exampleListOfLists;

    @DatabaseField
    private Map<TestEnum, String> exampleHashMap;

    public TestDatabaseDocument(Database database, String someKeyName) {
        super(database, Database.Collection.TEST_COLL, new Document().append("someKeyName", someKeyName));

        this.someKeyName = someKeyName;
    }

    @DatabaseParser(variableName = "exampleHashMap")
    private void parseExampleHashMap(Map<String, String> raw) {
        this.exampleHashMap = new HashMap<>();
        raw.forEach((key, value) -> this.exampleHashMap.put(TestEnum.valueOf(key), value));
    }

    @DatabaseSerializer(variableName = "exampleHashMap")
    private Map<String, String> serializeExampleHashMap() {
        HashMap<String, String> serialized = new HashMap<>();

        this.exampleHashMap.forEach((key, value) -> serialized.put(key.toString(), value));

        return serialized;
    }

    public static void main(String[] args) {
        Database database = new Database("mongodb://minecraft:Bl3X4titctsaIwXD@edgegen-shard-00-00-owl5a.mongodb.net:27017,edgegen-shard-00-01-owl5a.mongodb.net:27017,edgegen-shard-00-02-owl5a.mongodb.net:27017/test?ssl=true&replicaSet=EdgeGen-shard-0&authSource=admin&retryWrites=true&w=majority", "testdb");

        TestDatabaseDocument testDocument = new TestDatabaseDocument(database, "asdsaddsdsaasdaadsadsasdsa");
        testDocument.exampleHashMap = new HashMap<>();
        testDocument.exampleHashMap.put(TestEnum.TEST_ENUM_VALUE, "hey!!!");
//        testDocument.someValue = 6;
//        testDocument.anotherValue = "hey!";
//        testDocument.testEnum = TestEnum.TEST_ENUM_VALUE;
//        testDocument.exampleListOfLists = new ArrayList<>();
//        testDocument.exampleListOfLists.add(new ArrayList<>());
//        testDocument.exampleListOfLists.get(0).add("Hey!!!");
//        testDocument.exampleList.add("Hey!");
        testDocument.save();
    }
}
