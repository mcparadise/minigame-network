package me.chasertw123.minigames.shared.database;

import com.mongodb.client.MongoCursor;
import org.bson.Document;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class DatabaseDocument {

    private final Map<String, Object> loadDocumentState;
    private Database.Collection collection;
    private Database database;
    private Document queryDocument;

    public DatabaseDocument(Database database, Database.Collection collection, Document queryDocument) {
        this.loadDocumentState = new HashMap<>();
        this.collection = collection;
        this.database = database;
        this.queryDocument = queryDocument;

        this.load();
    }

    private List<Field> getDatabaseFields() {
        List<Field> fields = Arrays.stream(this.getClass().getDeclaredFields()).filter(field -> field.isAnnotationPresent(DatabaseField.class))
                .collect(Collectors.toList());

        fields.stream().filter(field -> !field.isAccessible()).forEach(field -> field.setAccessible(true)); // Make sure that we can access them

        return fields;
    }

    private Map<String, Method> getDatabaseSerializers() {
        List<Method> methods = Arrays.stream(this.getClass().getDeclaredMethods()).filter(method -> method.isAnnotationPresent(DatabaseSerializer.class))
                .collect(Collectors.toList());

        methods.stream().filter(method -> !method.isAccessible()).forEach(method -> method.setAccessible(true));

        Map<String, Method> methodMap = new HashMap<>();
        methods.forEach(method -> methodMap.put(method.getAnnotation(DatabaseSerializer.class).variableName(), method));

        return methodMap;
    }

    private Map<String, Method> getDatabaseParsers() {
        List<Method> methods = Arrays.stream(this.getClass().getDeclaredMethods()).filter(method -> method.isAnnotationPresent(DatabaseParser.class))
                .collect(Collectors.toList());

        methods.stream().filter(method -> !method.isAccessible()).forEach(method -> method.setAccessible(true));

        Map<String, Method> methodMap = new HashMap<>();
        methods.forEach(method -> methodMap.put(method.getAnnotation(DatabaseParser.class).variableName(), method));

        return methodMap;
    }

    public void load() {
        MongoCursor<Document> cursor = this.database.getMongoCollection(this.collection).find(this.queryDocument).iterator();
        Document document = cursor.hasNext() ? cursor.next() : new Document();
        Map<String, Method> databaseParsers = this.getDatabaseParsers();

        for (Field field : this.getDatabaseFields()) {
            try {
                if(document.get(field.getName()) != null) {
                    if(databaseParsers.containsKey(field.getName())) {
                        Method method = databaseParsers.get(field.getName());
                        Object output = document.get(field.getName());
                        method.invoke(this, method.getParameterTypes()[0].cast(output));
                    } else if(field.getType().isEnum()) {
                        field.set(this, Enum.valueOf((Class<Enum>) field.getType(), document.getString(field.getName())));
                    } else {
                        field.set(this, document.get(field.getName()));
                    }
                }
                loadDocumentState.put(field.getName(), field.get(this));
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public void save() {
        Document updateDocumentData = new Document();
        Map<String, Method> databaseSerializers = this.getDatabaseSerializers();

        // This will build a query based on the values that have changed
        for (Field field : this.getDatabaseFields()) {
            // Compare this to the start document state
            try {
                System.out.println("Field " + field.getName() + ", Field: " + field.get(this) + ", Original: " + loadDocumentState.get(field.getName()));

                boolean different = false;
                Object latestValue = field.get(this);

                if(latestValue != null && databaseSerializers.containsKey(field.getName()))
                    latestValue = databaseSerializers.get(field.getName()).invoke(this);

                Object originalValue = loadDocumentState.get(field.getName());

                if((latestValue == null && originalValue != null) || (originalValue == null && latestValue != null))
                    different = true;
                else if((latestValue != null && originalValue != null) && !latestValue.equals(originalValue))
                    different = true;

                if (different) {
                    updateDocumentData.put(field.getName(), field.getType().isEnum() ? latestValue.toString() : latestValue);
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        if(updateDocumentData.size() == 0) {
            System.out.println("Not updating as no diffs.");

            return;
        }

        System.out.println(updateDocumentData.toJson());

        // Don't save if it's just the key name that has updated / is from the original query
        if(updateDocumentData.equals(this.queryDocument)) {
            System.out.println("Not saving as is the query document");

            return;
        }

        // Create the query and execute
        this.database.getMongoCollection(this.collection).updateOne(this.queryDocument, new Document().append("$set", updateDocumentData), Database.upsert());
    }
}
