package me.chasertw123.minigames.shared.database;

import org.bson.Document;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ErrorPrintStream extends PrintStream {

    public ErrorPrintStream(PrintStream consoleOut, Database database) {
        super(new LauncherOutputStream(consoleOut, database));
    }

    private static class LauncherOutputStream extends OutputStream {

        private PrintStream consoleOut;
        private List<Integer> buildingMessage;
        private Database database;

        private LauncherOutputStream(PrintStream consoleOut, Database database) {
            this.consoleOut = consoleOut;
            this.database = database;

            this.buildingMessage = new ArrayList<>();
        }

        @Override
        public void write(int b) throws IOException {
            consoleOut.write(b); // Write this to console

            if(b == '\n') {
                // This is the end of a message, send it
                send();

                return;
            }

            buildingMessage.add(b);
        }

        private void send() {
            byte[] message = new byte[buildingMessage.size()];

            int i = 0;
            for(Integer c : buildingMessage)
                message[i++] = (byte) (char) (int) c;

            buildingMessage = new ArrayList<>(); // Reset it

            String stringMsg = new String(message, StandardCharsets.UTF_8);

            // Save this to mongo
            Document doc = new Document();
            doc.put("message", stringMsg);
            doc.put("timestamp", new Date().getTime());
            doc.put("server", ErrorLog.SERVER_NAME);
            database.getMongoCollection(Database.Collection.ERROR_LOGS).insertOne(doc);
        }
    }

}
