package me.chasertw123.minigames.core.api.scoreboard.animate;

public interface AnimatableString {

    String current();

    String next();

    String previous();

}
