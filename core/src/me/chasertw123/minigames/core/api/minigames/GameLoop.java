package me.chasertw123.minigames.core.api.minigames;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Chase on 7/30/2017.
 */
public abstract class GameLoop extends BukkitRunnable {

    public int interval;

    public GameLoop(int interval, long tickRate, Plugin plugin) {
        this.interval = interval;
        this.runTaskTimer(plugin, 0L, tickRate);
    }

    public GameLoop(int interval, long startDelay, long tickRate, Plugin plugin) {
        this.interval = interval;
        this.runTaskTimer(plugin, startDelay, tickRate);
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
