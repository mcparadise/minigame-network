package me.chasertw123.minigames.core.api.minigames.spectating;

public enum SpectateSetting {

    NIGHT_VISION,
    SEE_OTHERS

}
