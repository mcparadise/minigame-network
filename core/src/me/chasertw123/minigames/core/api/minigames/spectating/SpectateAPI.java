package me.chasertw123.minigames.core.api.minigames.spectating;

import me.chasertw123.minigames.core.Main;
import me.chasertw123.minigames.core.api.misc.Item;
import me.chasertw123.minigames.core.api.minigames.MinigameUtils;
import me.chasertw123.minigames.core.api.misc.AbstractGUI;
import me.chasertw123.minigames.core.user.User;
import me.chasertw123.minigames.shared.framework.ServerSetting;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class SpectateAPI {

    private static boolean canUseTeleporter = true;
    private static boolean canSeeOtherInventories = true;

    private static final List<UUID> spectators = new ArrayList<>();

    private static AbstractGUI spectatorGUI = new AbstractGUI((int) Math.max(1, Math.ceil((Bukkit.getOnlinePlayers().size() - spectators.size()) / 9d)), "Player's Remaining");

    private static PlayerAccessor playerAccessor = () -> Bukkit.getOnlinePlayers().stream()
            .map(Player::getUniqueId).filter(uuid -> !spectators.contains(uuid)).collect(Collectors.toList());
    private static SpectateItem spectateItem = (user) -> user.toItemStack().setDisplayName(user.getPlayer().getName())
            .setLore(ChatColor.WHITE + "Health: " + ChatColor.GREEN + (int) ((user.getPlayer().getHealth() / user.getPlayer().getMaxHealth()) * 100f) + "%");


    public static void setSpectating(User user) {
        spectators.add(user.getUUID());

        user.getPlayer().spigot().setCollidesWithEntities(false);
        user.setServerSetting(ServerSetting.DAMAGE, false);
        user.setServerSetting(ServerSetting.HUNGER, false);
        user.setServerSetting(ServerSetting.ITEM_DROPPING, false);

        MinigameUtils.resetPlayer(user.getPlayer(), true);

        Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), () -> {
            giveItems(user);
            Bukkit.getOnlinePlayers().stream()
                    .filter(player -> player.getUniqueId() != user.getUUID())
                    .forEach(player -> {
                        player.hidePlayer(user.getPlayer());
                        if (isPlayerSpectating(player))
                            user.getPlayer().hidePlayer(player);
                    });
        }, 2L);

        refreshTeleporterGUI();
    }

    public static void giveItems(User user) {
        Item.PLAYER_TELEPORTER.give(user.getPlayer(), 0);
        Item.PLAY_AGAIN.give(user.getPlayer(), 7);
        Item.RETURN_TO_HUB.give(user.getPlayer(), 8);
    }

    public static void refreshTeleporterGUI() {
        if (spectatorGUI == null || Math.ceil(spectatorGUI.getInventory().getSize() / 9d) > (int) Math.max(1, Math.ceil((Bukkit.getOnlinePlayers().size() - spectators.size()) / 9d))) {
            spectatorGUI = new AbstractGUI((int) Math.max(1, Math.ceil((Bukkit.getOnlinePlayers().size() - spectators.size()) / 9d)), "Player's Remaining");
            System.out.println(Math.ceil(spectatorGUI.getInventory().getSize() / 9d) + " > "
                    + (int) Math.max(1, Math.ceil((Bukkit.getOnlinePlayers().size() - spectators.size()) / 9d)));
        }

        spectatorGUI.getInventory().clear();

        int slot = 0;
        for (UUID uuid : playerAccessor.getPlayers()) {
            Player player = Bukkit.getPlayer(uuid);
            if (player == null)
                continue;

            User user = Main.getUserManager().getUser(player);
            spectatorGUI.setItem(spectateItem.createItemStack(user), slot++, (clickUser, clickType) -> {
                clickUser.getPlayer().teleport(user.getPlayer().getLocation());
                clickUser.getPlayer().closeInventory();
                clickUser.getPlayer().playSound(clickUser.getPlayer().getLocation(), Sound.CLICK, 1, 1);
            });
        }
    }

    public static boolean isPlayerSpectating(Player player) {
        return spectators.contains(player.getUniqueId());
    }

    public static void setPlayerAccessor(PlayerAccessor accessor)
    {
        playerAccessor = accessor;
    }

    public static void setSpectateItem(SpectateItem item) {
        spectateItem = item;
    }

    public static void setCanUseTeleporter(boolean canUse) {
        canUseTeleporter = canUse;
    }

    public static boolean canUseTeleporter() {
        return canUseTeleporter;
    }

    public static boolean canSeeOtherInventories() {
        return canSeeOtherInventories;
    }

    public static void setCanSeeOtherInventories(boolean canSeeOthers) {
        canSeeOtherInventories = canSeeOthers;
    }

    public static AbstractGUI getSpectatorGUI() {
        return spectatorGUI;
    }

    public interface PlayerAccessor { List<UUID> getPlayers(); }

    public interface SpectateItem { ItemStack createItemStack(User user); }
}
