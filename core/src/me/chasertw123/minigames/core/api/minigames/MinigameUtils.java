package me.chasertw123.minigames.core.api.minigames;

import me.chasertw123.minigames.core.Main;
import me.chasertw123.minigames.core.api.misc.AbstractItem;
import me.chasertw123.minigames.core.user.User;
import me.chasertw123.minigames.core.api.misc.cItemStack;
import me.chasertw123.minigames.shared.framework.ServerType;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.minecraft.server.v1_8_R3.EntityFireworks;
import net.minecraft.server.v1_8_R3.ItemStack;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.potion.PotionEffect;

import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

public class MinigameUtils {

    private final static Random rand = new Random();
    private final static HashMap<UUID, Long> lastInteraction = new HashMap<>();

    public static void showAllPlayers() {
        Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), () -> {
            for (Player player : Bukkit.getOnlinePlayers())
                for (Player other : Bukkit.getOnlinePlayers())
                    if (player.getUniqueId() != other.getUniqueId())
                        player.showPlayer(other);
        }, 5L);
    }

    public static String convertIntToOrdinal(int i) {

        String[] suffixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
        switch (i % 100) {

            case 11:
            case 12:
            case 13:
                return i + "th";

            default:
                return i + suffixes[i % 10];
        }
    }

    public static void resetPlayer(Player player) {
        resetPlayer(player, false);
    }

    public static void resetPlayer(Player player, boolean canFly) {
        for (PotionEffect effect : player.getActivePotionEffects())
            player.removePotionEffect(effect.getType());

        player.getInventory().setArmorContents(null);
        player.getInventory().clear();
        player.setLevel(0);
        player.setExp(0);
        player.setFireTicks(0);
        player.setFoodLevel(20);
        player.setMaxHealth(20d);
        player.setHealth(player.getMaxHealth());
        player.setSaturation(14.4F);
        player.setGameMode(GameMode.ADVENTURE);

        player.getPlayer().setAllowFlight(canFly);
        player.getPlayer().setFlying(canFly);
    }

    public static void playFireworks()
    {
        Bukkit.getOnlinePlayers().forEach(player ->
        {
            Location center = player.getLocation(), loc;
            double angle = rand.nextDouble() * 360;
            double x = center.getX() + (rand.nextDouble() * 32d * Math.cos(Math.toRadians(angle)));
            double z = center.getZ() + (rand.nextDouble() * 32d * Math.sin(Math.toRadians(angle)));
            loc = new Location(center.getWorld(), x, center.getY() + 2, z);

            FireworkEffect.Builder fwB = FireworkEffect.builder();
            fwB.flicker(rand.nextBoolean());
            fwB.trail(rand.nextBoolean());
            fwB.with(FireworkEffect.Type.values()[rand.nextInt(FireworkEffect.Type.values().length)]);
            fwB.withColor(Color.fromRGB(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256)));
            fwB.withFade(Color.fromRGB(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256)));
            FireworkEffect fe = fwB.build();

            ItemStack item = new ItemStack(net.minecraft.server.v1_8_R3.Items.FIREWORKS);

            FireworkMeta meta = (FireworkMeta) CraftItemStack.asCraftMirror(item).getItemMeta();
            meta.addEffect(fe);

            CraftItemStack.setItemMeta(item, meta);

            Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), () ->{
                EntityFireworks entity = new EntityFireworks(((CraftWorld) loc.getWorld()).getHandle(), loc.getX(), loc.getY(), loc.getZ(), item) {
                    @Override
                    public void t_() {
                        this.world.broadcastEntityEffect(this, (byte)17);
                        die();
                    }
                };

                entity.setInvisible(true);
                ((CraftWorld) loc.getWorld()).getHandle().addEntity(entity);
            }, 2L);
        });
    }

    public static class Messages
    {

        public static void sendHowToPlay(User user) {
            if (lastInteraction.containsKey(user.getUUID()) && lastInteraction.get(user.getUUID()) + 2500L > System.currentTimeMillis())
                return;

            lastInteraction.put(user.getUUID(), System.currentTimeMillis());
            user.getPlayer().spigot().sendMessage(new ComponentBuilder("How to Play").color(net.md_5.bungee.api.ChatColor.LIGHT_PURPLE).bold(true).append(" > ")
                    .color(net.md_5.bungee.api.ChatColor.DARK_GRAY).append("Click here to open a URL with how to play!").color(net.md_5.bungee.api.ChatColor.GRAY)
                    .bold(false).event(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://mcparadise.net/" + Main.getServerDataManager().getServerGameType()
                            .getPrefabName().replace('_', '-') + "/how-to-play")).create());
        }

        public static void sendGameOverMessage(User winner) {
            Main.getUserManager().getOnlinePlayers().forEach(user -> {
                user.sendCenteredMessage("&a&m-------------------------------------", true);
                user.sendCenteredMessage(" ", true);

                if (winner == null) {
                    user.sendCenteredMessage("&c&lGAME OVER! &eNO WINNERS!", true);
                } else if (user.getUUID() != winner.getUUID()) {
                    user.sendCenteredMessage("&c&lGAME OVER! &e" + winner.getPlayer().getName() + " WINS!", true);
                } else {
                    user.sendCenteredMessage("&c&lGAME OVER! &eYOU WIN!", true);
                }

                user.sendCenteredMessage("", true);
                user.sendCenteredMessage("&aYou earned a total of " + user.getGainedCoins() + " coins!", true);
                user.sendCenteredMessage("&bYou earned a total of " + user.getGainedXp() + " experience!", true);
                user.sendCenteredMessage("", true);
                user.sendCenteredMessage("&a&m-------------------------------------", true);

                Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), () -> {
                    user.getPlayer().sendMessage( ChatColor.RED + "Game Over " + ChatColor.GRAY + ">> " + ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Going to hub in 10 seconds.");

                    if (user.getRank().getRankType().getRankLevel() == 0) {
                        user.sendCenteredMessage("&d&m-------------------------------------", true);
                        user.sendCenteredMessage("&9" + user.getPlayer().getName() + "! &6Thanks for playing YANA!", true);
                        user.sendCenteredMessage("", true);
                        user.sendCenteredMessage("&f&lWe noticed you don't have a rank!", true);
                        user.sendCenteredMessage("&7Buy today to &cjoin priority queue, &7get a &ecolored name,", true);
                        user.sendCenteredMessage("&bmore friend slots, &7and &aeven more!", true);
                        user.sendCenteredMessage("", true);
                        user.sendCenteredMessage("&a&l >> YANA.gg <<", true);
                        user.sendCenteredMessage("&d&m-------------------------------------", true);
                    }
                }, 100L);
            });
        }

        public static void sendGameOverMessage(User[] winners)
        {
            // TODO: FOR TEAMS
        }
    }

}
