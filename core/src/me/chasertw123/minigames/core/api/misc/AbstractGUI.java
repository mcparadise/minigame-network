package me.chasertw123.minigames.core.api.misc;

import me.chasertw123.minigames.core.Main;
import me.chasertw123.minigames.core.user.User;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AbstractGUI {

    private static HashMap<UUID, AbstractGUI> openGUIs = new HashMap<>();

    private final Inventory inventory;
    private final Map<Integer, Interact> interactions = new HashMap<>();

    /**
     *
     * @param rows
     * @param title
     */
    public AbstractGUI(int rows, String title) {
        this.inventory = Bukkit.createInventory(null, rows * 9, title);
    }

    /**
     *
     * @param rows
     * @param title
     * @param user
     */
    public AbstractGUI(int rows, String title, User user) {
        this(rows, title);
        this.openInventory(user);
    }

    /**
     *
     * @param user
     */
    public void openInventory(User user) {
        user.getPlayer().openInventory(inventory);
        openGUIs.put(user.getUUID(), this);
        Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> user.getPlayer().updateInventory(), 2L);
    }

    /**
     *
     * @param slot
     * @return
     */
    public ItemStack getItemStack(int slot) {
        return inventory.getItem(slot);
    }

    /**
     * Get the Inventory
     *
     * @return Inventory contents
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Get the an {@link Interact} associated with a slot in the inventory
     *
     * @param slot The slot in the inventory
     * @return The action associated with the slot
     */
    public Interact getInteraction(int slot) {
        return interactions.get(slot);
    }

    /**
     * Set an item in the inventory with an action attached.
     *
     * @param item Item being set in the inventory
     * @param slot Slot where the item is set
     * @param interact The action taken when an item is clicked
     */
    public void setItem(ItemStack item, int slot, Interact interact) {
        inventory.setItem(slot, item);
        interactions.put(slot, interact);
    }

    /**
     * Set an item in the inventory with no action attached.
     *
     * @param item Item being set in the inventory
     * @param slot Slot where the item is set
     */
    public void setItem(ItemStack item, int slot) {
        inventory.setItem(slot, item);
        interactions.put(slot, null);
    }

    /**
     *
     * @param user
     */
    public void onClose(User user) { }

    /**
     * Interface to act as the action taken on click of an item
     */
    public interface Interact {
        void click(User user, ClickType clickType);
    }

    public static boolean hasOpenGUI(User user) {
        return openGUIs.get(user.getUUID()) != null;
    }

    public static AbstractGUI getGUI(User user) {
        return openGUIs.get(user.getUUID());
    }

    public static void closeGUI(User user) {

        if (!hasOpenGUI(user))
            return;

        openGUIs.get(user.getUUID()).onClose(user);
        openGUIs.remove(user.getUUID());
    }
}
