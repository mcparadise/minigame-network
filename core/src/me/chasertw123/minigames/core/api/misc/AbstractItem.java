package me.chasertw123.minigames.core.api.misc;

import me.chasertw123.minigames.core.user.User;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class AbstractItem {

    private static List<AbstractItem> abstractItems = new ArrayList<>();

    private final ItemStack item;
    private final AbstractAction action;

    public AbstractItem(ItemStack item, AbstractAction action) {
        this.item = item;
        this.action = action;

        if (!abstractItems.contains(this))
            abstractItems.add(this);
    }

    public ItemStack getItemStack() {
        return item;
    }

    public AbstractAction getAction() {
        return action;
    }

    public void give(Player player, int slot) {
        player.getInventory().setItem(slot, this.getItemStack());
        player.updateInventory();
    }

    public interface AbstractAction {
        void interact(User user, InteractType interactType);
    }

    public enum InteractType { LEFT, RIGHT, OTHER }

    public static AbstractItem getAbstractItem(ItemStack item) {
        return abstractItems.stream().filter(abstractItem -> abstractItem.getItemStack().getItemMeta()
                        .getDisplayName().equals(item.getItemMeta().getDisplayName())).findFirst().orElse(null);
    }
}
