package me.chasertw123.minigames.core.api.misc;

import me.chasertw123.minigames.core.Main;
import me.chasertw123.minigames.core.api.minigames.MinigameUtils;
import me.chasertw123.minigames.core.api.minigames.spectating.SpectateAPI;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Item {

    public static AbstractItem RETURN_TO_HUB = new AbstractItem(
            new cItemStack(Material.BED, ChatColor.RED + "Return to Hub " + ChatColor.GRAY + "(Right Click)"), (user, clickType) -> {
                user.sendPrefixedMessage(ChatColor.GREEN + "Sending to Hub...");
                user.sendToServer("hub");
    });

    public static AbstractItem PLAYER_TELEPORTER = new AbstractItem(
            new cItemStack(Material.COMPASS, ChatColor.AQUA + "Player Teleporter " + ChatColor.GRAY + "(Right Click)"), (user, clickType) -> {
        if (SpectateAPI.canUseTeleporter())
            SpectateAPI.getSpectatorGUI().openInventory(user);
    });

    public static AbstractItem SPECTATING_SETTINGS = new AbstractItem(
            new cItemStack(Material.REDSTONE_COMPARATOR, ChatColor.BLUE + "Spectating Settings " + ChatColor.GRAY + " (Right Click)"), (user, clickType) -> {
        user.sendMessage(ChatColor.RED + "Coming soon...");
    });

    public static AbstractItem PLAY_AGAIN = new AbstractItem(
            new cItemStack(Material.PAPER, ChatColor.GREEN + "Play Again " + ChatColor.GRAY + "(Right Click)"), (user, clickType) ->
            user.addToQueue(Main.getServerDataManager().getServerGameType().getPrefabName().replace('_', '-')));

    public static AbstractItem QUICK_PLAY = new AbstractItem(
            new cItemStack(Material.COMPASS, ChatColor.GREEN + "Arcade Games").addFancyLore("Quickly navigate to all of your favourite games!",
                    ChatColor.WHITE.toString()), (user, clickType) -> user.getPlayer().performCommand("ss"));

    public static AbstractItem HOW_TO_PLAY = new AbstractItem(new cItemStack(Material.ENCHANTED_BOOK, ChatColor.LIGHT_PURPLE + "How to Play "
            + ChatColor.GRAY + "(Right Click)"), (user, clickType) -> MinigameUtils.Messages.sendHowToPlay(user));

    public static ItemStack MAP_VOTING = new cItemStack(Material.EMPTY_MAP, ChatColor.YELLOW + "Map Voting " + ChatColor.GRAY + "(Right Click)");

    public static ItemStack HIDE_PLAYERS = new cItemStack(Material.PRISMARINE_SHARD, ChatColor.GREEN + "Player Visibility " + ChatColor.GRAY +
            "(Right-Click)").addFancyLore("Click to disable seeing other players and their effects. You will still be able to see your effects. " +
            "They should improve your performance.", ChatColor.GRAY.toString());

    public static ItemStack SHOW_PLAYERS = new cItemStack(Material.FLINT, ChatColor.RED + "Player Visibility " + ChatColor.GRAY + "(Right-Click)")
            .addFancyLore("Click to enable seeing other players and their effects. They may impact your performance.", ChatColor.GRAY.toString());

}
