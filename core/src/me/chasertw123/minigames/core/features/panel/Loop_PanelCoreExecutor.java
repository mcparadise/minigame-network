package me.chasertw123.minigames.core.features.panel;

import com.mongodb.Block;
import com.mongodb.client.model.Filters;
import me.chasertw123.minigames.core.Main;
import me.chasertw123.minigames.shared.database.Database;
import me.chasertw123.minigames.shared.framework.ServerType;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.Server;

import java.util.ArrayList;
import java.util.List;

public class Loop_PanelCoreExecutor implements Runnable {

    public Loop_PanelCoreExecutor() {
        Bukkit.getScheduler().scheduleAsyncRepeatingTask(Main.getInstance(), this, 0, 20 * 5);
    }

    @Override
    public void run() {
        String serverName = Main.getServerDataManager().getServerName();

        if(serverName == null && Main.getServerDataManager().getServerType() != ServerType.HUB)
            return; // This takes time to generate.
        else if (Main.getServerDataManager().getServerType() == ServerType.HUB)
            serverName = "hub";

        // Convert the server name - we need it in short form
        if(!serverName.equals("hub")) {
            String cleaned = serverName.replace("-", "").replace(".", "");
            serverName = cleaned.substring(Math.max(0, cleaned.length() - 8));
        }

        Document query = new Document();
        query.put("target", serverName);
        query.put("completed", false);

        // Check for messages
        Main.getMongoDatabase().getMongoCollection(Database.Collection.PANEL_MESSAGES)
                .find(query).forEach((Block<? super Document>) record -> {
            if(record.getBoolean("completed"))
                return;

            String command = record.getString("command");
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);

            record.put("completed", true);

            // Delete the record
            Main.getMongoDatabase().getMongoCollection(Database.Collection.PANEL_MESSAGES).deleteOne(Filters.eq("uuid", record.getString("uuid")));
        });
    }
}
