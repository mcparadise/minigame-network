package me.chasertw123.minigames.core.features.chests;

import me.chasertw123.minigames.core.api.misc.AbstractGUI;
import me.chasertw123.minigames.core.user.User;

public class GUI_Chest extends AbstractGUI {

    public GUI_Chest(User paradisePlayer) {
        super(3, "Select Chest");

        for(ChestType chestType : ChestType.values())
            setItem(chestType.getItemStack(paradisePlayer), chestType.getSlot(), (user1, clickType) -> {

            });
    }

}
