package me.chasertw123.minigames.core.listeners.minigame;

import me.chasertw123.minigames.core.Main;
import me.chasertw123.minigames.core.api.minigames.spectating.SpectateAPI;
import me.chasertw123.minigames.core.api.misc.AbstractGUI;
import me.chasertw123.minigames.core.api.misc.cItemStack;
import me.chasertw123.minigames.core.event.UserChatEvent;
import me.chasertw123.minigames.core.user.User;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import java.util.stream.Collectors;

public class Events_SpectatorMode implements Listener {

    @EventHandler (priority = EventPriority.LOWEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (!event.isCancelled()
                && SpectateAPI.isPlayerSpectating(event.getPlayer())
                && event.getAction() == Action.RIGHT_CLICK_BLOCK)
            event.setCancelled(true);
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (!event.isCancelled()
                && event.getDamager() instanceof Player
                && SpectateAPI.isPlayerSpectating((Player) event.getDamager()))
            event.setCancelled(true);
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        if (!event.isCancelled() && SpectateAPI.isPlayerSpectating(event.getPlayer()))
            event.setCancelled(true);
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onUserChatEvent(UserChatEvent event) {
        if (event.isCancelled() || !SpectateAPI.isPlayerSpectating(event.getUser().getPlayer()))
            return;

        event.setRecipients(event.getRecipients().stream().filter(user -> SpectateAPI.isPlayerSpectating(user.getPlayer())
                || user.getRank().isStaff()).collect(Collectors.toList()));

        TextComponent formattedMessage = new TextComponent(new ComponentBuilder("[SPEC] ").color(ChatColor.RED).create());
        formattedMessage.addExtra(event.getSenderMessage());

        event.setSenderMessage(formattedMessage);
        event.setRecipientMessage(formattedMessage);
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event) {
        if (!event.isCancelled()
                && event.getTarget() instanceof Player
                && SpectateAPI.isPlayerSpectating((Player) event.getTarget()))
            event.setCancelled(true);
    }

    @EventHandler (priority = EventPriority.LOW)
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (event.isCancelled()
                || !SpectateAPI.canSeeOtherInventories()
                || !(event.getRightClicked() instanceof Player)
                || !SpectateAPI.isPlayerSpectating(event.getPlayer()))
            return;

        Player other = (Player) event.getRightClicked();
        AbstractGUI otherInventory = new AbstractGUI(6, other.getName() + "'s Inventory", Main.getUserManager().getUser(event.getPlayer())) {
            @Override
            public void onClose(User user) {
                user.getPlayer().playSound(user.getPlayer().getLocation(), Sound.CHEST_CLOSE, 0.5f, 1f);
            }
        };

        for (int i = 0; i <= 8; i++) {
            otherInventory.setItem(new cItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15, " "), i);
            otherInventory.setItem(new cItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15, " "), 36 + i);
            otherInventory.setItem(other.getInventory().getItem(i), 45 + i);
        }

        for (int i = 9; i <= 35; i++)
            otherInventory.setItem(other.getInventory().getItem(i), i);

        otherInventory.setItem(other.getInventory().getHelmet() != null ? other.getInventory().getHelmet()
                : new cItemStack(Material.BARRIER, ChatColor.RED + "No Helmet Equipped"), 1);
        otherInventory.setItem(other.getInventory().getChestplate() != null ? other.getInventory().getChestplate()
                : new cItemStack(Material.BARRIER, ChatColor.RED + "No Chestplate Equipped"), 3);
        otherInventory.setItem(other.getInventory().getLeggings() != null ? other.getInventory().getLeggings()
                : new cItemStack(Material.BARRIER, ChatColor.RED + "No Leggings Equipped"), 5);
        otherInventory.setItem(other.getInventory().getBoots() != null ? other.getInventory().getBoots()
                : new cItemStack(Material.BARRIER, ChatColor.RED + "No Boots Equipped"), 7);

        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.CHEST_OPEN, 0.5f, 1f);
    }

}
