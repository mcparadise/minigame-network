package me.chasertw123.minigames.core.listeners;

import me.chasertw123.minigames.core.Main;
import me.chasertw123.minigames.core.api.misc.AbstractItem;
import me.chasertw123.minigames.core.user.User;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class AbstractItem_Events implements Listener {

    @EventHandler (priority = EventPriority.LOWEST)
    public void onPlayerInteract(PlayerInteractEvent event) {

        if (event.getPlayer().getItemInHand() == null || event.getPlayer().getItemInHand().getType() == Material.AIR)
            return;

        AbstractItem item = AbstractItem.getAbstractItem(event.getPlayer().getItemInHand());
        if (item == null)
            return;

        User user = Main.getUserManager().getUser(event.getPlayer());
        AbstractItem.InteractType interactType = AbstractItem.InteractType.OTHER;

        switch (event.getAction()) {
            case RIGHT_CLICK_AIR:
            case RIGHT_CLICK_BLOCK:
                interactType = AbstractItem.InteractType.RIGHT;
                break;

            case LEFT_CLICK_AIR:
            case LEFT_CLICK_BLOCK:
                interactType = AbstractItem.InteractType.LEFT;
                break;
        }

        item.getAction().interact(user,interactType);
        event.setCancelled(true);
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onInventoryClick(InventoryClickEvent event) {

        if (event.isCancelled()
                || !(event.getWhoClicked() instanceof Player)
                || event.getCurrentItem() == null
                || event.getCurrentItem().getType() == Material.AIR)
            return;

        AbstractItem item = AbstractItem.getAbstractItem(event.getCurrentItem());
        if (item == null)
            return;

        User user = Main.getUserManager().getUser((Player) event.getWhoClicked());
        AbstractItem.InteractType interactType = AbstractItem.InteractType.OTHER;

        switch (event.getClick()) {
            case LEFT:
            case SHIFT_LEFT:
                interactType = AbstractItem.InteractType.LEFT;
                break;

            case RIGHT:
            case SHIFT_RIGHT:
                interactType = AbstractItem.InteractType.RIGHT;
                break;
        }

        item.getAction().interact(user,interactType);
        event.setCancelled(true);
    }

}
