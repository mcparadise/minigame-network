package me.chasertw123.minigames.core.listeners;

import me.chasertw123.minigames.core.Main;
import me.chasertw123.minigames.core.api.misc.AbstractGUI;
import me.chasertw123.minigames.core.user.User;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class AbstractGUI_Events implements Listener {

    @EventHandler (priority = EventPriority.HIGHEST)
    public void onInventoryClick(InventoryClickEvent event) {

        if (event.isCancelled()
                || !(event.getWhoClicked() instanceof Player)
                || event.getCurrentItem() == null
                || event.getCurrentItem().getType() == Material.AIR)
            return;

        User user = Main.getUserManager().getUser((Player) event.getWhoClicked());
        if (!AbstractGUI.hasOpenGUI(user))
            return;

        AbstractGUI.Interact interaction = AbstractGUI.getGUI(user).getInteraction(event.getSlot());
        if (interaction != null)
            interaction.click(user, event.getClick());

        event.setCancelled(true);
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onInventoryClose(InventoryCloseEvent event) {
        User user = Main.getUserManager().getUser((Player) event.getPlayer());
        if (AbstractGUI.hasOpenGUI(user))
            AbstractGUI.closeGUI(user);
    }

}
