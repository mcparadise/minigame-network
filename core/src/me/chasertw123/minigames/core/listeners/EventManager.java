package me.chasertw123.minigames.core.listeners;

import me.chasertw123.minigames.core.Main;
import me.chasertw123.minigames.core.api.CoreAPI;
import me.chasertw123.minigames.core.listeners.general.*;
import me.chasertw123.minigames.core.listeners.general.packets.Event_NamedSoundEffect;
import me.chasertw123.minigames.core.listeners.general.packets.Event_TabComplete;
import me.chasertw123.minigames.core.listeners.minigame.Events_SpectatorMode;
import me.chasertw123.minigames.shared.framework.ServerType;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Scott Hiett on 6/27/2017.
 */
public class EventManager {

    private Listener[] minigameListeners = {
            new Event_PlayerJoin(),
            new Events_SpectatorMode()
    };

    private Listener[] generalListeners = {
            new Event_AsyncPlayerChat(),
            new Event_EntityDamage(),
            new Event_FoodLevelChange(),
            new Event_LeavesDecay(),
            new Event_PlayerCommandPreprocess(),
            new Event_PlayerQuit(),
            new Event_PlayerDropItem(),
            new Event_PlayerJoin(),

            new AbstractGUI_Events(),
            new AbstractItem_Events()
    };

    public EventManager() {

        Event_MessageListener messageListener = new Event_MessageListener();
        new Event_TabComplete();
        new Event_NamedSoundEffect();

        Main.getInstance().getServer().getMessenger().registerOutgoingPluginChannel(Main.getInstance(), "BungeeCord");
        Main.getInstance().getServer().getMessenger().registerOutgoingPluginChannel(Main.getInstance(), "mcparadise:bridge");

        Main.getInstance().getServer().getMessenger().registerIncomingPluginChannel(Main.getInstance(), "mcparadise:bridge", messageListener);
        Main.getInstance().getServer().getMessenger().registerIncomingPluginChannel(Main.getInstance(), "BungeeCord", messageListener);

        List<Listener> activeListeners = new ArrayList<>(Arrays.asList(generalListeners));

        // TODO: Remove after testing
//        if (Main.getAPI().getServerType() == ServerType.HUB) {
//            Chest.addChest(new Chest(new Location(Bukkit.getWorld("world"), 34D, 95D, -21D), new Location(Bukkit.getWorld("world"), 37.5, 95.0, -20.5, 90, 20)));
//        }

        // Set this as else ->

        if (CoreAPI.getServerDataManager().getServerType() == ServerType.MINIGAME)
            activeListeners.addAll(Arrays.asList(minigameListeners));

        for (Listener listener : activeListeners)
            Bukkit.getServer().getPluginManager().registerEvents(listener, Main.getInstance());
    }
}
